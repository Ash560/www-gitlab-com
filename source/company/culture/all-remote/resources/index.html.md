---
layout: markdown_page
title: "Remote-work resources"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Introduction

On this page, we're curating a list of resources for remote professionals, teams, and organizations.

## Resources on GitLab's all-remote approach

#### Articles

- [How GitLab Cracked The Code Of Remote Working](https://www.tfir.io/2019/05/12/how-gitlab-cracked-the-code-of-remote-working-gitlab-contribute-report/)
- [A guide to remote working for startups](https://davidmytton.blog/a-guide-to-remote-working-for-startups/)
- [The office of the future is no office at all, says startup - Wall Street Journal](https://www.wsj.com/articles/the-office-of-the-future-is-no-office-at-all-says-startup-11557912601 )
- [8 secrets of GitLab's remote work culture](https://www.tfir.io/2019/05/15/8-secrets-of-gitlabs-remote-work-success/)
- [How to keep healthy communication habits in remote teams](https://medium.com/gitlab-magazine/how-to-keep-healthy-communication-habits-in-remote-teams-a19eca371952)
- [Remote work demands a supportive company culture](https://insights.dice.com/2019/01/22/remote-work-demands-supportive-company-culture/)
- [5 fun strategies companies are using to make remote workers feel included](https://business.linkedin.com/talent-solutions/blog/employee-engagement/2019/strategies-companies-use-to-keep-remote-workers-feeling-included)
- [Tech's newest unicorn has employees in 45 countries and zero offices](https://qz.com/work/1394496/gitlab-techs-newest-unicorn-has-no-offices/)
- [No need to come to the office: Making remote work at GitLab](https://hackernoon.com/no-need-to-come-to-the-office-making-remote-work-at-gitlab-737c42865210)
- [GitLab and Buffer CEOs talk transparency at scale](/blog/2017/03/14/buffer-and-gitlab-ceos-talk-transparency/)
- [Why GitLab is remote only](http://tapes.scalevp.com/remote-only-gitlab-sytse-sid-sijbrandij/)

#### Blogs from GitLab team members

- [5 tips for mastering video calls](/blog/2019/08/05/tips-for-mastering-video-calls/)
- [How to make your home a space that works with kids](/blog/2019/08/01/working-remotely-with-children-at-home/)
- [How being all remote helps us practice our values at GitLab](/blog/2019/07/31/pyb-all-remote-mark-frein/)
- [How I balance a baby, a career at GitLab, and cultural expectations of motherhood](/blog/2019/07/25/balancing-career-and-baby/)
- [How to live your best remote life](/blog/2019/07/09/tips-for-working-from-home-remote-work/)
- [5 things you might hear when meeting with GitLab's CEO](/blog/2019/06/28/five-things-you-hear-from-gitlab-ceo/)
- [How I work from anywhere (with good internet)](/blog/2019/06/25/how-remote-work-at-gitlab-enables-location-independence/)
- [A day in the life of the "average" remote worker](/blog/2019/06/18/day-in-the-life-remote-worker/)
- [One year at GitLab](http://shedoesdatathings.com/post/1-year-at-gitlab/)
- [How we turned a dull weekly all-hands into a podcast](/blog/2019/06/03/how-we-turned-40-person-meeting-into-a-podcast/)
- [The GitLab handbook by the numbers](/blog/2019/04/24/the-gitlab-handbook-by-numbers/)
- [Remote manifesto by GitLab](/blog/2015/04/08/the-remote-manifesto/)
- [Working remotely at GitLab: an update](/blog/2016/03/04/remote-working-gitlab/)
- [The case for all-remote companies](/blog/2018/10/18/the-case-for-all-remote-companies/)
- [How remote work enables rapid innovation at GitLab](/blog/2019/02/27/remote-enables-innovation/)
- [Why GitLab pays local rates](/blog/2019/02/28/why-we-pay-local-rates/)
- [Remote work: 9 tips for eliminating distractions and getting things done](/blog/2018/05/17/eliminating-distractions-and-getting-things-done/)
- [A day in the life of a remote Sales Development Representative](/blog/2018/05/11/day-in-life-of-remote-sdr/)
- [The remote future: How tech companies are helping their remote teams connect](/blog/2018/04/27/remote-future-how-remote-companies-stay-connected/)
- [Remote work, done right](/blog/2018/03/16/remote-work-done-right/)
- [How working at GitLab has changed my view on work and life](/blog/2018/03/15/working-at-gitlab-affects-my-life/)
- [GitLab team-members share how to recognize burnout and how to prevent it](/blog/2018/03/08/preventing-burnout/)
- [How we stay connected as a remote company](/blog/2016/12/05/how-we-stay-connected-as-a-remote-company/)
- [How to become the best distributed software development team](/blog/2017/09/15/pick-your-brain-interview-kwan-lee/)
- [How to keep remote (volunteer) teams engaged](/blog/2016/12/21/how-to-keep-remote-teams-engaged/)
- [3 things I learned in my first month at GitLab](/blog/2016/11/02/three-things-i-learned-in-my-first-month-at-gitlab/)
- [Working at GitLab: 30 days later](/blog/2016/11/03/working-at-gitlab-30-days-later/)
- [What it's like to be a parent working at GitLab](/blog/2016/04/08/remote-working-parents/)
- [To work remotely you need: Wifi and good communication habits](/blog/2016/03/23/remote-communication/)
- [Highlights to my first remote job](/blog/2015/06/17/highlights-to-my-first-remote-job/)
- [I'm leaving GitLab to help everyone work remotely](https://medium.com/@jobv/im-leaving-gitlab-to-help-everyone-work-remotely-1f6828ec45d5)

#### Videos, podcasts, interviews, presentations

- [YouTube: TFiR Interview - He Built A $1 Billion Open Source Company With No Headquarters: Sid Sijbrandij of GitLab](https://www.youtube.com/watch?v=ha4aMKl3MRA)
- [How to build and manage a distributed team - GitLab and Arch Systems](https://youtu.be/tSp5se9BudA)
- [Building a distributed company](https://outklip.com/blog/gitlab-building-a-distributed-company/)
- [Using video for effective collaboration for remote teams](https://youtu.be/6mZqzK_40FE)
- [GitLab’s secret to managing 160 employees in 160 locations - Interview by Y Combinator](https://blog.ycombinator.com/gitlab-distributed-startup/)
- [GitLab and Turtle's CEOs talk about running remote companies](https://youtu.be/-23DGyIMorE)
- [GitLab's remote-only presentation, 2017](https://docs.google.com/presentation/d/1JHHYQvAhsudGz8QB8nqp5ScJjqyhPD3ehCoKOlZb7VE/edit#slide=id.g1d6fee80ee_0_348)
- [Opening keynote from GitLab Contribute, 2019](https://youtu.be/kDfHy7cv96M?t=735)
- [Software Engineering Daily: GitLab with Sid Sijbrandij](https://softwareengineeringdaily.com/2019/03/15/gitlab-with-sid-sijbrandij/)
- [The Changelog: GitLab's Master Plan](https://changelog.com/podcast/220)
- [This Week In Tech: GitLab](https://twit.tv/shows/floss-weekly/episodes/473)
- [Outklip Blog: Using Video for Remote Work: Q&A with GitLab CEO](https://outklip.com/blog/using-video-for-remote-work/)
- [The Twenty Minute VC: A podcast interview with GitLab founder Sid Sijbrandij](http://www.thetwentyminutevc.com/sidsijbrandij/)
- [YouTube: All remote work with Mark Frein (InVision) & Sid Sijbrandij (GitLab)](https://www.youtube.com/watch?v=IFBj9KQSQXA)
- [Coder Radio interview with Sid on 100% remote work and the GitLab model](https://coder.show/313?t=1099)
- [YouTube: Sid presents Remote Work Best Practices at Mesosphere](https://www.youtube.com/watch?v=UFhHetf7kHM)

#### Threads and conversations

- [HackerNews thread about all-remote page](https://news.ycombinator.com/item?id=19785262)
- [HackerNews thread about why more companies aren't remote](https://news.ycombinator.com/item?id=20104173)

## Resources on remote work

#### Articles 

- [Why remote work leads to a stronger company culture](https://www.inc.com/brian-de-haaff/why-remote-work-leads-to-a-stronger-company-culture.html)
- [How companies benefit when employees work remotely](https://hbswk.hbs.edu/item/how-companies-benefit-when-employees-work-remotely)
- [Why working from home is a “future-looking technology”](https://www.gsb.stanford.edu/insights/why-working-home-future-looking-technology)
- [The firm with 900 staff and no office](https://www.bbc.com/news/business-48879976)
- [The distributed workplace](https://medium.com/@markfrein70/the-distributed-workplace-15ef447fa926)
- [Why office hours are obsolete](https://www.forbes.com/sites/soulaimagourani/2019/06/25/why-office-hours-are-obsolete/#715b7e007585)
- [It’s time to factor remote work into our urban planning](https://qz.com/work/1641664/remote-workers-are-the-solution-to-urban-crowding/)
- [23 key remote work and telecommuting statistics for 2019](https://www.owllabs.com/blog/remote-work-statistics)
- [How remote work can reduce stress and revitalize your mindset](https://thriveglobal.com/stories/how-remote-work-can-reduce-stress-and-revitalize-your-mindset/)
- [The 1 trait all remote workers need -- here is how to cultivate it](https://www.inc.com/brian-de-haaff/the-one-trait-all-remote-workers-need-here-is-how-to-cultivate-it.html)
- [The Financial Benefits of Working Remotely (for Companies and Employees)](https://www.creativelive.com/blog/financial-benefits-of-working-remotely/)
- [6 ways in which offering flexible working makes you a great employer](https://www.business2community.com/human-resources/6-ways-in-which-offering-flexible-working-makes-you-a-great-employer-02209279)
- [5 reasons remote teams are more engaged than office workers](https://www.business.com/articles/remote-workers-more-engaged/)
- [Remote work becoming "new normal"](https://finance.yahoo.com/news/remote-working-becoming-normal-190509351.html)
- [Here's why remote workers are more productive than in-house teams](https://www.forbes.com/sites/abdullahimuhammed/2019/05/21/heres-why-remote-workers-are-more-productive-than-in-house-teams/)
- [The benefits of and questions facing remote and distributed startups](https://tomtunguz.com/remote-and-fully-distributed/)
- [The end of an era for easyDNS](https://easydns.com/blog/2019/05/23/the-end-of-an-era-for-easydns/)
- [Engagement around the world, charterd - HBR](https://hbr.org/2019/05/engagement-around-the-world-charted)
- [Helping Creativity Happen from a Distance](https://distributed.blog/2019/07/25/helping-creativity-happen-from-a-distance/)
- [How to build a remote team](https://decryptmedia.com/7052/how-to-build-a-remote-team)
- [Remote work doesn't scale... or does it?](https://medium.com/@getadam/remote-work-doesnt-scale-or-does-it-4a72ce2bb1f3)
- [Case Closed: Work-From-Home is the World's Smartest Management Strategy](https://www.inc.com/geoffrey-james/case-closed-work-from-home-is-worlds-smartest-management-strategy.html)
- [A Long Commute Could Be the Last Thing Your Marriage Needs](https://www.forbes.com/sites/markeghrari/2016/01/21/a-long-commute-could-be-the-last-thing-your-marriage-needs/#5baf10f04245)
- [6 People Who Prove You Don’t Have to Sacrifice Your Career to Work Remotely](https://doist.com/blog/remote-career-advice/)
- [After Growing to 50 People, We’re Ditching the Office Completely](https://open.buffer.com/no-office/)
- [On-Premise Tribes in Shiny Caves](https://medium.com/understanding-as-a-service-uaas/on-premise-people-and-shiny-caves-remote-as-a-service-97cff86382b6)
- [Being tired isn’t a badge of honor](https://m.signalvnoise.com/being-tired-isn-t-a-badge-of-honor-fa6d4c8cff4e)
- [The benefits - and pitfalls - of working in isolation](http://theconversation.com/the-benefits-and-pitfalls-of-working-in-isolation-105350)
- [It's not just the isolation. Working from home has surprising downsides](https://theconversation.com/its-not-just-the-isolation-working-from-home-has-surprising-downsides-107140)
- [The Day They Invented Offices](https://shift.infinite.red/a-hypothetical-conversation-with-a-real-estate-developer-in-a-world-without-offices-53cd7be0942#.pufgl7l3a)
- [That remote work think piece has some glaring omissions (a rant)](http://www.catehuston.com/blog/2016/04/07/that-remote-work-think-piece-has-some-glaring-omissions/)
- [Tweets about the impact by Amir Salihefendic](https://twitter.com/amix3k/status/881251640795439104)
- [Martin Fowler on remote vs. co-located](https://martinfowler.com/articles/remote-or-co-located.html)
- [Top 5 Reasons Why Companies Adopt Remote Work](https://www.remoter.com/top-5-reasons-why-companies-adopt-remote-work)
- [Remote Work 1-2-3: First Three Steps to Going Remote](https://www.remoter.com/remote-work-1-2-3-first-three-steps-to-going-remote)
- [Work is Work: In which returns diminish](https://codahale.com/work-is-work/)

#### Best practices

- [Hiring top talent from generation Z: 14 essential recruitment tips](https://www.forbes.com/sites/forbeshumanresourcescouncil/2019/08/05/hiring-top-talent-from-generation-z-14-essential-recruitment-tips/)
- [Remote Ideation: Synchronous vs. Asynchronous](https://www.nngroup.com/articles/synchronous-asynchronous-ideation/)
- [How to create genuine camaraderie among remote workers](https://thriveglobal.com/stories/how-to-create-genuine-camaraderie-among-remote-workers/)
- [How remote businesses can attract high-quality job candidates](https://www.business.com/articles/remote-business-quality-hires/)
- [Six lessons we learned while scaling a distributed startup](https://www.forbes.com/sites/forbestechcouncil/2019/07/30/six-lessons-we-learned-while-scaling-a-distributed-startup/)
- [How to inspire and motivate your team that works from home](https://www.forbes.com/sites/dedehenley/2019/07/27/how-to-inspire-and-motivate-your-team-that-works-from-home/)
- [How to separate home-life from working-from-home life](https://www.searchenginejournal.com/home-life-working-from-home/317995/#close)
- [How these 5 secrets help remote workers thrive](https://thriveglobal.com/stories/how-these-5-secrets-help-remote-workers-thrive/)
- [5 ways to cultivate a healthy mindset in a remote team](https://thriveglobal.com/stories/5-ways-to-cultivate-a-healthy-mindset-in-a-remote-team/)
- [Remote work is here to stay: Here's how to avoid three common compliance issues](https://www.forbes.com/sites/forbeshumanresourcescouncil/2019/07/12/remote-work-is-here-to-stay-heres-how-to-avoid-three-common-compliance-issues/)
- [How to lead those working remotely](https://thriveglobal.com/stories/how-to-lead-those-working-remotely/)
- [How to build a successful remote business](https://www.forbes.com/sites/forbesbusinessdevelopmentcouncil/2019/07/11/how-to-build-a-successful-remote-business)
- [How to stay productive when you're working remotely](https://thriveglobal.com/stories/working-remotely-from-home-stay-productive-tips/)
- [10 tips for improving your remote conversations](https://www.smartbrief.com/original/2019/07/10-tips-improving-your-remote-conversations)
- [450 remote workers reveal how to boost productivity when working from home](https://thriveglobal.com/stories/450-remote-workers-reveal-how-to-boost-productivity-when-working-from-home/)
- [Managing Remote Employees: Best Practices from Doist’s Head of Marketing](https://doist.com/blog/best-practices-managing-remote-employees/)
- [The future of work is distributed. Here's how your company can strategize.](https://www.forbes.com/sites/falonfatemi/2019/06/28/the-future-of-work-is-distributed-heres-how-your-company-can-strategize)
- [Leading remotely - What every manager should know](https://thriveglobal.com/stories/leading-remotely-what-every-manager-should-know/)
- [Why engaging remote workers is good people business and how to do it](https://www.forbes.com/sites/forbeshumanresourcescouncil/2019/06/17/why-engaging-remote-workers-is-good-people-business-and-how-to-do-it/)
- [How to Pitch Your Boss on a Remote Working Arrangement](https://www.creativelive.com/blog/how-to-pitch-boss-on-remote-working/)
- [4 golden rules for living the laptop lifestyle](https://www.forbes.com/sites/stephanieburns/2019/06/14/4-golden-rules-for-living-the-laptop-lifestyle/)
- [5 ways to make your remote team more effective](https://www.forbes.com/sites/serenitygibbons/2019/06/13/5-ways-to-make-your-remote-team-more-effective/)
- [How to make remote work, work](https://www.inc.com/shama-hyder/how-to-make-remote-work-work.html)
- [5 ways to stay motivated when you work remote](https://www.forbes.com/sites/stephanieburns/2019/05/30/5-ways-to-stay-motivated-when-you-work-remote/)
- [Five mistakes that destroy the efficiency of your remote workers](https://www.forbes.com/sites/forbescoachescouncil/2019/05/29/five-mistakes-that-destroy-the-efficiency-of-your-remote-workers)
- [These Are the 8 Best MacOS Apps for Working Remotely](https://www.inc.com/jason-aten/these-are-8-best-macos-apps-for-working-remotely.html)
- [Remote working tips by Groove](https://www.groovehq.com/blog/remote-work-tips)
- [Guidelines for Effective Collaboration](https://github.com/ride/collaboration-guides)
- [The Ultimate Guide to Remote Standups](http://blog.idonethis.com/ultimate-guide-remote-standups/)
- [How Do You Manage Global Virtual Teams?](https://en.wikibooks.org/wiki/Managing_Groups_and_Teams/How_Do_You_Manage_Global_Virtual_Teams%3F)
- [Guide to leading and managing distributed teams](http://schoolofherring.com/2015/09/02/guide-to-leading-and-managing-distributed-teams/)
- [Getting Virtual Teams Right](https://hbr.org/2014/12/getting-virtual-teams-right)
- [Introverts at Work: Designing Spaces for People Who Hate Open-Plan Offices](http://www.bloomberg.com/news/articles/2014-06-16/open-plan-offices-for-people-who-hate-open-plan-offices)
- [How to get the most out of employees who work from home](https://www.entrepreneur.com/article/228752)
- [How to manage employees who work from home](https://money.cnn.com/2013/08/27/smallbusiness/employees-work-from-home/index.html)
- [How to manage a distributed development team](https://www.cio.com/article/2399319/how-to-manage-a-distributed-development-team.html)
- [Managing Remote Developer Teams: How Buffer Set the Gold Standard - Interview with Katie Womersley ](https://codingsans.com/blog/managing-remote-developer-teams)
- [What Is the Best Compensation Model?](https://www.remoter.com/what-is-the-best-compensation-model)


#### Surveys and data

- [Why remote work has grown by 159% since 2005](https://www.techrepublic.com/article/why-remote-work-has-grown-by-159-since-2005/)
- [Google spent 2 years researching what makes a great remote team](https://www.inc.com/justin-bariso/google-spent-2-years-researching-what-makes-a-great-remote-team-it-came-up-with-these-3-things.html)
- [State of Remote Work 2019 by Buffer](https://buffer.com/state-of-remote-work-2019)
- [The IWG Global Workspace Survey - Welcome to Generation Flex](http://assets.regus.com/pdfs/iwg-workplace-survey/iwg-workplace-survey-2019.pdf)
- [5 Important Takeaways From Google's Two-Year Study Of Remote Work](https://www.forbes.com/sites/abdullahimuhammed/2019/05/18/5-important-takeaways-from-googles-two-year-study-of-remote-work/#56e705797439)
- [Study: Adding 20 Minutes to Your Commute Makes You as Miserable as Getting a 19 Percent Pay Cut](https://www.inc.com/business-insider/study-reveals-commute-time-impacts-job-satisfaction.html)
- [Remote Work 2020 Survey by Remote.Tools](https://remotework2020.remote.tools/)

#### Videos, interviews, presentations

- [LinkedIn's 2019 Global Talent Trends report](https://business.linkedin.com/content/dam/me/business/en-us/talent-solutions/resources/pdfs/global-talent-trends-2019.pdf)
- [Things I Wish I Knew Before Going Remote by Marla Brizel Zeschin](https://www.youtube.com/watch?v=nUQ41-vBtdg)
- [Bond Internet Trends 2019: Remote work = creating internet-enabled work opportunities + efficiencies](https://www.bondcap.com/report/itr19/#view/228)
- [Work Remotely: Thrive in a Job From Home](https://www.creativelive.com/class/work-remotely-thrive-in-a-job-from-home-with-darren-murph)
- [Founding and Growing Remotely Online Course](https://www.remoter.com/remoter-course-founding-and-growing-remotely)

#### Threads and conversations

- [#remotechat - remote-focused Twitter chat every Wednesday](https://twitter.com/search?q=%23remotechat&src=typeahead_click&f=live)
- [Tweet thread about downsides of remote work](https://twitter.com/rakyll/status/1143271423722455040?s=09)
- [HackerNews thread about remote work literature](https://news.ycombinator.com/item?id=20884974)

#### Tools that enable remote teams

- [Slack](https://slack.com) - chat
- [Zoom](https://zoom.com) - video calls
- [8x8](https://www.8x8.com/) - unified communications (video, VoIP, etc.)
- [G-Suite](https://gsuite.google.com/)
- [GitLab Issues](https://docs.gitlab.com/ee/user/project/issues/)
- [GitLab Pages](/product/pages/)
- [There](https://there.pm/) - quick time zone reference for your global team
- [Remo](https://remo.co/) - virtual workspace tool
- [CoScreen](https://www.coscreen.co/) - remote screensharing
- [Figma](https://www.figma.com/) - design collaboration 

Here's [a list](/handbook/tools-and-tips/) of the tools we use internally at GitLab, with details on how we use them.

#### Organizations for traveling remote work

- [Remote Year](http://www.remoteyear.com/)
- [Wifi Tribe](https://wifitribe.co/)
- [The Remote Experience](https://www.theremoteexperience.com/)
- [Hacker Paradise](https://www.hackerparadise.org/)
- [Wifly Nomads](https://www.wiflynomads.com/)
- [Co-work Paradise](http://www.coworkparadise.com/)
- [Project Getaway](http://www.projectgetaway.com/)
- [B-Digital Nomad](https://www.b-digitalnomad.com/)
- [Digital Outposts](http://digitaloutposts.com/)
- [Remote Work Association](https://www.remoteworkassociation.com/)

#### Guides for remote work
- [Google's Distributed Work Playbook](http://services.google.com/fh/files/blogs/distributedworkplaybooks.pdf)
- [The Art of Working Remotely](https://artofworkingremotely.com)
- [Twist's Remote Work Guides](https://twist.com/remote-work-guides)
- [Remote Habits](https://remotehabits.com/)
- [35 of the best Slack communities for remote workers](https://www.owllabs.com/blog/remote-work-slack-communities?hs_amp=true)
- [The Remoter Guide](https://www.remoter.com/remoter-guide)

## Contribute to this page

At GitLab, we recognize that the whole idea of all-remote organizations is still
quite new, and can only be successful with active participation from the whole community.
Here's how you can participate:

- Propose or suggest any change to this site by creating a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests/).
- [Create an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues/) if you have any questions or if you see an inconsistency.
- Help spread the word about all-remote organizations by sharing it on social media.

----

Return to the main [all-remote page](/company/culture/all-remote/).
