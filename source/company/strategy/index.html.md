---
layout: markdown_page
title: "GitLab Strategy"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Organization

GitLab's mission, vision, strategy, and planning follow a [cadence](/handbook/ceo/cadence).
Along each time period, we answer various fundamental questions: why, what, how,
and when. The matrix of questions vs time period is below, with mappings to the
appropriate sections of this page.

| [Time Period](/handbook/ceo/cadence/) | 30 Years | 10 Years | 3 Years | 1 Year |
|--------------------------------------:|:--------:|:--------:|:-------:|:------:|
| Why | [Why](#why) |  |  |  |
| What | [Mission](#mission), [BHAG](#big-hairy-audacious-goal) | [Vision](#vision),  [Goals](#goals) | [Strategy](#strategy) |  |
| How | [Values](#values) | [How](#how) | [Principles](#principles), [Assumptions](#assumptions), [Pricing](#pricing), [Challenges](#challenges-as-we-grow-and-scale), [Dual Flywheels](#dual-flywheels), [KPIs](#publicly-viewable-okrs-and-kpis) |  |
| When |  |  | [Sequence](#sequence) | [Plan](#plan) |

## Why

We believe in a world where **everyone can contribute**. We believe that all
digital products should be open to contributions; from legal documents to movie
scripts, and from websites to chip designs.

Allowing everyone to make a proposal is the core of what a DVCS
([Distributed Version Control System](https://en.wikipedia.org/wiki/Distributed_version_control))
such as Git enables. No invite needed: if you can see it, you can contribute.

We think that it is logical that our collaboration tools are a collaborative
work themselves. More than [2,000 people](http://contributors.gitlab.com/) have
contributed to GitLab to make that a reality.

## Mission

Therefore, it is GitLab's mission to change all creative work from read-only to
read-write so that **everyone can contribute**.

When **everyone can contribute**, consumers become contributors and we greatly
increase the rate of human progress.

## Values

Our mission guides our path, and we live our [values](/handbook/values/) along this path.

## Vision

In summary, our vision is as follows:

GitLab Inc. develops great open source software to enable people to collaborate
in this way. GitLab is a [single application](/handbook/product/single-application/)
based on
[convention over configuration](/handbook/product/#convention-over-configuration)
that everyone should be able to afford and adapt. With GitLab, **everyone can
contribute**.

## Big Hairy Audacious Goal

Our [BHAG](https://www.jimcollins.com/concepts/bhag.html) over 
[the next 30 years](/handbook/ceo/cadence/#mission) 
is to become
the most popular collaboration tool for knowledge workers in any industry. For
this, we need to make the DevOps lifecycle much more user friendly.

## How

Everyone can contribute to digital products with GitLab, to GitLab itself, and to our organization.

1. To ensure that **everyone can contribute with GitLab** we allow anyone to create a proposal, at any time, without setup, and with confidence. Let's analyze that sentence a bit.

   - Anyone: Every person in the world should be able to afford great DevOps software. GitLab.com has free private repos and CI runners and GitLab CE is [free as in speech and as in beer](http://www.howtogeek.com/howto/31717/what-do-the-phrases-free-speech-vs.-free-beer-really-mean/). But open source is more than a license, that is why we are [a good steward of GitLab CE](/company/stewardship/) and keep both GitLab CE and EE open to inspection, modifications, enhancements, and suggestions.
   - Create: It is a [single application](/handbook/product/single-application/) based on [convention over configuration](/handbook/product/#convention-over-configuration).
   - Proposal: With Git, if you can read it, you can fork it to create a proposal.
   - At any time: you can work concurrently to other people, without having to wait for permission or approval from others.
   - Without setup: You can make something without installing or configuring for hours with our web IDE and Auto DevOps.
   - With confidence: Reduce the risk of a flawed proposal with review apps, CI/CD, code quality, security scans, performance testing, and monitoring.

1. To ensure that **everyone can contribute to GitLab, the application** we
   actively welcome contributors. We do this by having quality code, tests,
   documentation, popular frameworks, and offering a comprehensive [GitLab Development Kit](https://gitlab.com/gitlab-org/gitlab-development-kit)
   and a dedicated [GitLab Design System](https://design.gitlab.com/). We use
   GitLab at GitLab Inc., we [dogfood](/handbook/product/#dogfood-everything) it
   and make it a tool we continue to love. We celebrate contributions by
   recognizing a Most Valuable Person (MVP) every month. We allow everyone to
   anticipate, propose, discuss, and contribute features by having everything on
   a public issue tracker. We ship a new version every month so contributions
   and feedback are visible fast. To contribute to open source software, people
   must be empowered to learn programming. That is why we sponsor initiatives
   such as Rails Girls.

   There are a few significant, but often overlooked, nuances of the **everyone can contribute to GitLab, the application** mantra:

   * While collaboration is a core value of GitLab, over collaborating tends to involve team members unnecessarily, leading to consensus-based decision making, and ultimately slowing the pace of improvement in the GitLab application. Consider [doing it yourself](/handbook/values/#collaboration), creating a merge request, and facilitating a discussion on the solution.
   * For valuable features in line with our product philosophy, that do not yet exist within the application, don't worry about UX having a world class design before shipping. While we must be good stewards of maintaining a quality product, we also believe in rapid iteration to add polish and depth after an [MVC](/handbook/product/#the-minimally-viable-change-mvc) is created.
   * Prefer creating merge requests ahead of issues in order to suggest a tangible change to facilitate collaboration, driving conversation to the recommended implementation.
   * Contributors should feel free to create what they need in GitLab. If quality engineering requires charting features, for example, which would normally be implemented out of another team, they should feel empowered to prioritize their own time to focus on this aspect of the application.
   * GitLab maintainers, developers, and Product Managers should be viewed as coaches for contributions, independent of source. While there are contributions that may not get merged as-is (such as copy/paste of EE code into the CE code base or features that aren't aligned with product philosophy), the goal is to coach contributors to contribute in ways that are cohesive to the rest of the application.

   A group discussion reiterating the importance of everyone being able to contribute:
   <figure class="video_container">
   <iframe src="https://www.youtube.com/embed/l374J98iOmk?t=675" frameborder="0" allowfullscreen="true" width="640" height="360"> </iframe>
   </figure>

1. To ensure that **everyone can contribute to GitLab the company** we have open
   business processes that allow all team members to suggest improvements to our
   handbook. We hire remotely so everyone with an internet connection can come
   work for us and be judged on results, not presence in an office. We offer
   equal opportunity for every nationality. We are agnostic to location and
   create more equality of opportunity in the world. We engage on Hacker News,
   Twitter, and our blog post comments. And we strive to take decisions guided
   by [our values](/handbook/values).

## Goals

1. Ensure that **everyone can contribute** in the 3 ways outlined above.

2. Become most used software for the software development lifecycle and collaboration on all digital content by following [the sequence below](#sequence).

3. Complete our [product vision](/direction/#vision) of a [single application](/handbook/product/single-application/) based on [convention over configuration](/handbook/product/#convention-over-configuration).

4. Offer a sense of progress [in a supportive environment with smart colleagues](http://pandodaily.com/2012/08/10/dear-startup-genius-choosing-co-founders-burning-out-employees-and-lean-vs-fat-startups/).

5. Stay independent so we can preserve our values. Since we took external investment, we need a [liquidity event](https://en.wikipedia.org/wiki/Liquidity_event). To stay independent, we want to become a [public company](/handbook/being-a-public-company/) instead of being acquired.

## Risks

We acknowledge the risks in achieving our goals. We documents them in our our [biggest risks page](/handbook/leadership/biggest-risks/).

## Strategy

Along the road to realizing our mission of **everyone can contribute**, our
strategic goal is to become the leading, if not the only, complete DevOps
platform delivered as a [single application](/handbook/product/single-application/). We believe we can achieve this due to the [dual flywheels](#dual-flywheels) of our open-core model.

As we execute on our strategy, it's important to keep in mind that [cost goals are not due to going public](/handbook/being-a-public-company/#cost-goals-are-not-due-to-going-public)

More detail on our product strategy can be found on our [direction page](/direction/#vision).

## Sequence

Our strategy is on a [3 year cadence](/handbook/ceo/cadence/#strategy).

Our near team goal is to **[go public](/handbook/being-a-public-company/) in CY2020**, specifically on Wednesday November 18, 2020 which is five years after the first people got stock options with 4 years of vesting. November 18th, 2020 was also set for a number of other fun, [company culture](/company/culture/#planned-date-of-november-18th-2020-to-take-gitlab-public) related reasons.

By November 18, 2023 we want to:

1. **Grow Incremental ACV**, resulting in over $1B ARR, and be cash flow positive.
1. **Popular next generation product** where [50%](/company/strategy/2023/) of our [categories](/handbook/product/categories/) are at [lovable maturity](/direction/maturity) and with [100m SMAU](/handbook/product/growth/#smau).
1. **Highly performant team** with top talent [team-member satisfaction](/handbook/business-ops/data-team/metrics/#satisfaction) above 90% and more than 90% of top talent answering "My team is a highly performing team." in the affirmative.

The top 3 objectives of [our OKRs](/company/okrs/) have matched the bold part of this strategy since we started our OKRs in [CY2017-Q3](/company/okrs/2017-q3/).

## Breadth over depth

We realize our competitors have started earlier and have more capital. Because we started later we need a more compelling product that covers the complete [scope](/direction/#scope) with a [single application](/handbook/product/single-application/) based on [convention over configuration](/handbook/product/#convention-over-configuration) in a cloud native way. Because we have less capital, we need to build that as a community. Therefore it is important to share and ship our [vision for the product](/direction/#vision). The people that have the most knowledge have to prioritize **breadth over depth** since only they can add new functionality. Making the functionality more comprehensive requires less coordination than making the initial minimal feature. Shipping functionality that is incomplete to expand the scope sometimes goes against our instincts. However leading the way is needed to allow others to see our path and contribute. With others contributing, we'll iterate faster to [improve and polish functionality over time](http://mark.pundsack.com/2017/03/14/Polygonal-Product-Management/). So when in doubt, the rule of thumb is breadth over depth, so everyone can contribute.

If you want an analogy think of our product team as a plow way in front that tills the earth. It takes a while for the plants (complete features) to grow behind it. This tilled earth is ugly to look at but it surfaces the nutrients that the wider community needs to be inspired and to contribute.

If we can make a product that is strong with all features from planning to monitoring, and it works well, then we believe we can become the number one solution that companies standardize around. We need to offer the benefits that you can only have with an integrated product.

So breadth over depth is the strategy for GitLab the company. GitLab the project should have depth in every category it offers. It will take a few years to become [best in class in a certain space](/is-it-any-good/#gitlab-ci-is-a-leader-in-the-the-forrester-wave) because we depend on users contributing back, and we publish that journey on our [maturity page](/direction/maturity/). But that is the end goal, an application of unmatched breadth and depth.

## Principles

1. Independence: since we took financing we need to have a [liquidity event](https://en.wikipedia.org/wiki/Liquidity_event); to maintain independence we want to become a public company rather than be acquired.

1. Low burn: spend seed money like it is the last we’ll raise, maintain 2 years of runway.

1. First time right: last to market so we get it right the first time, a fast follower with taste.

1. Values: make decisions based on [our values](/handbook/values), even if it is inconvenient.

1. Reach: go for a broad reach, no focus on business verticals or certain programming languages.

1. Breadth over depth: See [Breadth over depth](#breadth-over-depth).

1. Speed: ship every change in the next release to maximize responsiveness and learning.

1. Life balance: we want people to stay with us for a long time, so it is important to take time off, work on life balance, and being remote-only is a large part of the solution.

## Assumptions

1. [Open source user benefits](http://buytaert.net/acquia-retrospective-2015): significant advantages over proprietary software because of its faster innovation, higher quality, freedom from vendor lock-in, greater security, and lower total cost of ownership.

2. [Open Source stewardship](/company/stewardship/): community comes first, we [play well with others](/handbook/product/#plays-well-with-others) and share the pie with other organizations commercializing GitLab.

3. [Innersourcing](/blog/2014/09/05/innersourcing-using-the-open-source-workflow-to-improve-collaboration-within-an-organization/) is needed and will force companies to choose one solution top-down.

4. Git will dominate the version control market in CY2020.

5. A single application where [interdependence creates exceptional value](https://medium.com/@gerstenzang/developer-tools-why-it-s-hard-to-build-a-big-business-423436993f1c#.ie38a0cls) is superior to a collection of tools or a network of tools. Even so, good integrations are important for network effects and making it possible to integrate GitLab into an organization.

6. To be sustainable we need an open core model that includes a proprietary GitLab EE.

7. EE needs a low base price that is publicly available to compete for reach with CE, established competitors, and new entrants to the market.

8. The low base price for EE is supplemented by a large set of options aimed at larger organizations that get a lot of value from GitLab.

## Pricing

Most of GitLab functionality is and will be available for free in Core. Our paid
tiers include features that are
[more relevant for managers, directors, and executives](/company/stewardship/#what-features-are-paid-only).
[We promise](/company/stewardship/#promises) all major features in [our scope](/direction/#scope)
are available in Core too. Instead of charging for specific parts of our scope
(CI, Monitoring, etc.) we charge for smaller features that you are more likely
to need if you use GitLab with a lot of users. There are a couple of reasons for
this:

1. We want to be a good [steward of our open source product](/company/stewardship/).
1. Giving a great free product is part of our go to market, it helps create new users and customers.
1. Having our scope available to all users increases adoption of our scope and helps people see the benefit of an [single application](/handbook/product/single-application/).
1. Including all major features in Core helps reduce merge conflicts between CE and EE

Because we have a great free product we can't have one price. Setting it high
would make the difference from the free version too high. Setting it low would
make it hard to run a sustainable business. There is no middle ground that would
work out with one price.

That is why we have a [Starter, Premium, and Ultimate tiers](/handbook/product/#paid-tiers).
The price difference between each of them is half an order of magnitude (5x).

We charge for making people more effective and will charge per user, per
application, or per instance. We do include free minutes with our subscriptions
and trials to make it easier for users to get started. As we look towards more
deployment-related functionality on .com it's tempting to offer compute and
charge a percent on top of, for example, Google Cloud Platform (GCP). We don't
want to charge an ambiguous margin on top of another provider since this limits
user choice and is not transparent. So we will always let you BYOK (bring your
own Kubernetes) and never lock you into our infrastructure to charge you an
opaque premium on those costs.

## Customer acceptance

We firmly adhere to laws [including trade compliance laws](/handbook/people-group/code-of-conduct/#trade-compliance-exportimport-control) in countries where we do business, and welcome everyone abiding by those legal restrictions to be customers of GitLab. In some circumstances, we may opt to not work with particular organizations, on a case-by-case basis. Some reasons we may choose not to work with certain entities include, but are not limited to:

1. Engaging in illegal, unlawful behavior.
1. Making derogatory statements or threats toward our community.
1. Encouraging violence or discrimination against legally protected groups.

This policy is in alignment with our mission, contributor and employee code-of-conduct and company values. Here are some links that may give you some background at how we arrived at this customer acceptance policy:

* Our mission is "everyone can contribute." This mission is in alignment with our open source roots and the [MIT license](https://en.wikipedia.org/wiki/MIT_License) our open source software is subject to. The MIT license is a free software license that allows the freedom to run the program as you wish, for any purpose. This informs our customer acceptance policy.

* GitLab has a [contributor code of conduct](/community/contribute/code-of-conduct/) for _how_ to contribute to GitLab, but there are no restrictions on _who_ can contribute to GitLab. We desire that everyone can contribute, as long as they abide by the code of conduct.

* GitLab has a set of values for how Gitlabbers strive to conduct themselves. We don’t expect all companies to value collaboration, results, efficiency, diversity, inclusion and transparency in the same way we do. As an open company, “everyone can contribute” is our default and [transparency](/handbook/values/#transparency) is our check and balance. Transparency means our handbook, issues, merge requests and product roadmap are online for everyone to see and contribute to.

* Related topic: At GitLab, we want to avoid an environment where people feel alienated for their religious or political opinions. Therefore, we encourage Gitlabbers to refrain from taking positions on specific [religious or political issues](/handbook/values/#religion-and-politics-at-work-) in public company forums (such as on the GitLab Contribute stage, the daily company call or Slack channels) because it is easy to alienate people that may have a minority opinion. It is acceptable to bring up these topics in social contexts such as coffee chats and real-life meetups with other coworkers, but always be aware of cultural sensitivities, exercise your best judgement, and make sure you stay within the boundaries of our  [code of conduct](/handbook/people-group/code-of-conduct/). We always encourage [discussion and iteration](/handbook/values/#anyone-and-anything-can-be-questioned) on any company policy, including this one.

## Challenges as we grow and scale

Losing the interest of the open source community would be detrimental to our success. For example, if someone wanted to make a contribution to CE and decided not to merge it because a similar feature already existed in EE, we would have lost out on an important contribution from the community.

We'll also need to adapt with a changing market. Netflix is a great example of this. Everyone knew that video on demand was the future. Netflix, however, started shipping DVDs over mail. They knew that it would get them a database of content that people would want to watch on demand. Timing is everything.

If a new, better version control technology dominates the market, we will need to adopt it and keep an open mind. Hopefully, we will be big enough at that point that people will consider us an integrated DevOps product. If not, we can always change our name, but we are currently investing to make git better for everyone.

For more thoughts on pricing please see our [pricing model](/handbook/ceo/pricing/).

## Dual flywheels

GitLab has two flywheel strategies that reinforce each other: our open core flywheel and our development spend flywheel.
A flywheel strategy is [defined as](https://medium.com/evergreen-business-weekly/flywheel-effect-why-positive-feedback-loops-are-a-meta-competitive-advantage-6d0ed55b67c5) one that has a positive feedback loops that build momentum, increasing the payoff of incremental effort.
You can visualize how the flywheels work in congruence via the diagram below. The KPI and responsibilities table lists the relevant indicator and department for every part of the flywheel.

```mermaid
graph BT;
  id1(More Users)-->id2(More Revenue);
  id2(More Revenue)-->id3(More Features);
  id3(More Features)-.->id1(MoreUsers);
  id1(More Users)-.->id4(More Contributions);
  id3(More Features)-->id1(More Users);
  id4(More Contributions)-.->id3(More Features);
```

### KPIs and Responsible departments

| Part of flywheel | Key Performance Indicator (KPI) | Department |
|-------------- ---|---------------------------------|------------|
| More Users | Stage Monthly Active Users | Product |
| More Contributions | Wider community contributions per release | Community Relations |
| More Features | Merge Requests per release per engineer in product development | Engineering and Product Management |
| More Revenue | IACV vs. plan | Sales |

## Publicly viewable OKRs and KPIs

To make sure our goals are clearly defined and aligned throughout the organization, we make use of [Objectives and Key Results (OKRs)](/company/okrs/) and [Key Performance Indicators (KPIs)](/handbook/ceo/kpis/) which are both publicly viewable.

## Plan

Our near terms plans are captured on our [direction page](/direction).

## Why is this page public?

Our strategy is completely public because transparency is one of our [values](/handbook/values).
We're not afraid of sharing our strategy because, as Peter Drucker said,
"Strategy is a commodity, execution is an art."
