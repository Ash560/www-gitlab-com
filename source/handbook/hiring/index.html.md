---
layout: handbook-page-toc
title: "Hiring"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Hiring pages

- [Greenhouse](/handbook/hiring/greenhouse/)
- [Principles](/handbook/hiring/principles/)
- [Job families](/handbook/hiring/job-families/)
- [Vacancies](/handbook/hiring/vacancies/)
- [Recruiting Alignment](/handbook/hiring/recruiting-alignment/)
- [Interviewing](/handbook/hiring/interviewing/)
- [Sourcing](/handbook/hiring/sourcing/)
- [Job offers and post-interview processes](/handbook/hiring/offers/)
- [Recruiting Process Framework](/handbook/hiring/recruiting-framework/)
- [Referral Process](/handbook/hiring/referral-process/)
- [Recruiting Metrics Process](/handbook/hiring/metrics/)
- [Hiring Charts](/handbook/hiring/charts/)
- [Preferred Companies to Recruit from](/handbook/hiring/preferred-companies/)

Potential applicants should refer to the [jobs FAQ page](/jobs/faq/).

## Related to hiring

- [Current vacancies](/jobs/apply/)
- [Contracts](/handbook/contracts)
- [Benefits](/handbook/benefits/)
- [Compensation](/handbook/people-group/global-compensation/)
- [Stock options](/handbook/stock-options)
- [Visas](/handbook/people-group/visas/)
- [Background checks](/handbook/people-group/code-of-conduct/#background-checks)
- [Onboarding](/handbook/general-onboarding)

## Definitions

Job families and vacancies are different things and can't be used interchangeably. Hopefully this information will help to clarify the differences.

- A [job family](/job-families) is a permanent item; its content is a superset of all vacancies for the job family, and it is created with a merge request. Since it is permanent please don't include text that becomes outdated when we hire someone, for example: "We are seeking".
- A [vacancy](/handbook/hiring/vacancies/) is a temporary item posted on Greenhouse; its content is a subset of the job family, and it is created by copying parts of a job family based on an issue.

We don't use the word "job" to refer to a job family or vacancy because it is ambiguous.

People at GitLab can be a specialist on one thing and expert in many:

- A [specialization](/company/team/structure/#specialist) is specific to a job family, each team member can have only one, it defined on the relevant job family page. A specialist uses the compensation benchmark of the job family.
- An [expertise](/company/team/structure/#expert) is not specific to a job family, each team member can have multiple ones, the expertises a team member has are listed on our team page.

The example below shows how we describe what someone does at GitLab:

```
1. Level: Senior
1. Job family: Developer
1. Specialist: Gitaly specialist
1. Location: EMEA
1. Expert: Reliability, Durability
```

We use the following terms to refer to a combination of the above:

### Title

Level and job family as listed on the contract, for example: Senior Developer
 
### Headline

All parts except expertise, as listed on vacancies, for example: Senior Developer, Gitaly specialist, EMEA

Please use the same order as in the examples above, a few notes:

- Level comes before job family.
- Specialist comes after job family and always includes 'specialist'.
- Location comes after specialization.

We preface a title with "interim" when we're hiring for the position.

## Equal Employment Opportunity

 Diversity & Inclusion is one of GitLab's core [values](/handbook/values) and
 GitLab is dedicated to providing equal employment opportunities (EEO) to all team members
 and candidates for employment without regard to race, color, religion, gender,
 national origin, age, disability, or genetics. One example of how put this into practice
 is through sponsorship of [diversity events](/blog/2016/03/24/sponsorship-update/)

 GitLab complies with all applicable laws governing nondiscrimination in employment. This policy applies to all terms and conditions of employment, including recruiting, hiring, placement, promotion, termination, layoff, recall, transfer,
 leaves of absence, compensation, and training. GitLab expressly prohibits any form of workplace harassment.
 Improper interference with the ability of GitLab’s team members to perform their role duties
 may result in discipline up to and including discharge. If you have any complaints, concerns,
 or suggestions to do better please [contact People Business Partner](/handbook/people-group/#reach-peopleops).

## Country Hiring Guidelines

Please go to [country hiring guidelines](/jobs/faq/#country-hiring-guidelines) for more information.

## Hiring best practices for hiring managers

### Optimize the job description

* Reread the job description multiple times. Read it forward and read it in reverse to catch mistakes. Ask your team to review it. Poorly written job descriptions will not attract candidates.
* Check your job description for [gender coding](http://gender-decoder.katmatfield.com/), so you can attract diverse candidates.
* Consider [recording a video](https://about.gitlab.com/handbook/hiring/vacancies/#consider-a-role-specific-video-overview) describing the role and adding it to the job description.

### Optimize job postings

* Select the geographic regions via analyzing where the candidates are that have the skills you seek and what the comp ratio is for that region using the trends feature via using features of LinkedIn recruiter. An example for Ruby on Rails developers can be found in this [sheet - accessible internally only](https://docs.google.com/spreadsheets/d/1v3O-luqOpSJINFhEQnjA8-zgCIKDcL5gVuuXP9TeE6s/edit#gid=0).  If you don't have LinkedIn recruiter access, reach out to your recruiter to find out how to be granted access.
* Search in LinkedIn Recruiter to determine if your job is listed correctly and in the geographic regions you want to find candidates (LinkedIn doesn't have a no-location/remote feature). Some candidates may be interested in remote jobs but may only be searching in their geographic region. If you find problems with this, reach out to your recruiter to resolve.  To do this in LinkedIn Recruiter:
  * Do a search for the candidates with the skill(s) you are seeking
  * Click on "View search insights"
  * Click on "Location"
  * If the results are not granular enough for your needs (for examples shows "India" rather than the regions in India), do a search for each country separately.
* Consider also listing your role on [Fossjobs.net](https://www.fossjobs.net/), [Torre.co](https://torre.co), and [AngelList](https://angel.co/).
* Don't spend much time on reviewing your job listings on Glassdoor or Indeed. They haven't proven to be effective tools for finding candidates for GitLab.

### Promote jobs

* Encourage your team and team's stakeholders to promote the jobs via social media posts (LinkedIn, Twitter, etc.). 
* By generating sharable links from Greenhouse (through the "Share Jobs with your Social Network" option on your dashboard) referrals will be tracked back to the team member who shared them.

### Source candidates
* Encourage your team and the team's stakeholders to refer candidates. It is recommended that the team member use Greenhouse to refer the candidate (rather than the candidate applying and putting the team member's name in the referrer field). Doing so allows the team member to view/track the candidates interviewing process status.
* Do a [source-a-thon](https://about.gitlab.com/handbook/hiring/sourcing/#source-a-thons) with your team to help the sourcing team find more candidates.

### Optimize the recruiting process
* Encourage interviewers to use [Calendly](https://calendly.com/) with the recruiting scheduler to schedule interviews. Doing so reduces the number of steps the candidate and GitLab needs to do to schedule an interview. Using a paid account is encouraged so that multiple different meeting lengths can be scheduled.
* Review all candidates weekly in Greenhouse to confirm that no tasks have been dropped. This can happen due to the number of hand-offs between people and due to the large number of steps to be managed by the recruiting team.
* Coordinate with other interviewers to ensure that the interview process covers a range of scenarios and minimizes duplication of questions.
* If there is a reasonably high likelihood of a candidate passing the next scheduled interview, ask the coordinator to schedule the next interview after the currently scheduled one. That parallelizes the process and reduces the total time to vet candidates.

### Optimize the reference check process 
 
* Do reference checks via audio or video rather than email. This tends to provide more useful feedback on candidates. Using a 15 minute Calendly meeting to coordinate this is recommended.
* Encourage the candidate to tell their references that they will be hearing from you. This increases the likelihood that they will respond.
* In the email to the reference, include the candidate's full name and purpose of the email (to schedule a reference call).  This also increases the likelihood that they will respond.
