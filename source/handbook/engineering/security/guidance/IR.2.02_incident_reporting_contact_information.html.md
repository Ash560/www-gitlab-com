---
layout: handbook-page-toc
title: "IR.2.02 - Incident Reporting Contact Information Control Guidance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Control Statement
GitLab provides a contact method for external parties to:

* Submit complaints and inquiries
* Report incidents

## Context
Having an easily accessible and public channel for external parties to contact GitLab in the event of a security incident provides a way for the community to help GitLab keep its systems safe and to faster identify and respond to security incidents internally. This control can be tested by means of citing sufficient documentation with respect to emergency contacts and on-call engineers to support when an incident occurs, and to see if this documentation is easily available.  

## Scope
This control applies to GitLab.com

## Ownership
* Control Owner: `Corporate Compliance`
* Process owner(s):
    * Security Operations
    * Infrastructure
    * Legal

## Additional control information and project tracking
Non-public information relating to this security control as well as links to the work associated with various phases of project work can be found in the [Incident Reporting Contact Information control issue](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/issues/843).

Examples of evidence an auditor might request to satisfy this control:
* Handbook pages that provide external parties a contact method
* Link to the `gitlab-foss` issue tracker and samples of relevant issues reporting incidents

### Policy Reference
GitLab provides a contact method for external parties to:

1. Submit complaints and inquiries
*  [Incident Management](https://about.gitlab.com/handbook/support/incident-management/)
*  [On-Call Runbooks](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/incident-management/#on-call-runbooks). Links to incident response runbooks for on-call engineers.

*  [Support Team function in the handbook](https://about.gitlab.com/handbook/support/) 
*  [Support page](https://about.gitlab.com/support/) contains information to contact the Support team

*  [Incident Slack Tooling](#5543)
*  [Security Incident Comms Plan](gitlab-com/gl-security/operations#205)
*  [Security Incident Comms Plan](gitlab-com/www-gitlab-com!29911)

2. Report incidents
*  [Process for engaging security on-call](https://about.gitlab.com/handbook/engineering/security/#engaging-the-security-on-call)
*  [Security operations on-call guide](https://about.gitlab.com/handbook/engineering/security/secops-oncall.html#gitlab-security-operations-on-call-guide)
*  [Security Incident Comms Plan](https://gitlab.com/gitlab-com/gl-security/secops/operations/issues/205)

3. GitLab IR Contact information in the Handbook
*  [Information on how to contact the GitLab legal team](https://about.gitlab.com/handbook/legal/)
*  [Information on how to submit DMCA takedown requests to GitLab](https://about.gitlab.com/handbook/engineering/security/dmca-removal-requests.html)
*  [GitLab maintains current contact information for external parties to report Security incidents](https://about.gitlab.com/handbook/engineering/security/#external-contact-information)
*  [GitLab maintains an active bug bounty program](https://about.gitlab.com/handbook/engineering/security/#vulnerability-reports-and-hackerone) on HackerOne as another way for external parties to report security vulnerabilities
*  [All other inquiries and reports can be made on the `gitlab-ce` issue tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues)

4. [Red Team Rules of Engagement](https://about.gitlab.com/handbook/engineering/security/red-team/red-team-roe.html)
5. [Incident Management for Self-Managed Customers](https://about.gitlab.com/handbook/support/incident-management/)


## Framework Mapping
* ISO
  * A.16.1.2
* SOC2 CC
  * CC2.3
* PCI
  * 12.10.1
