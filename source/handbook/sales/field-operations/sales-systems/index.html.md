---
layout: handbook-page-toc
title: "Sales Systems"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Welcome to the Sales Systems Handbook

### Charter
* To support the Gitlab sales organization by providing a functional, stable and reliable platform for the teams to operate within. 
* To provide a user experience that enhances the various team members workflows
* To build the foundation of a scalable system that enables to continued growth of the sales team

### Sales Systems Issues
*  Create an issues in the [Sales Systems](https://gitlab.com/gitlab-com/sales-team/field-operations/systems/issues) project

### Technical Documentation
*  [Go-To-Market Technical Documentation](/handbook/sales/field-operations/sales-systems/gtm-technical-documentation/)
