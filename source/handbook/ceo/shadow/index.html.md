---
layout: handbook-page-toc
title: "CEO Shadow Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction
{:.no_toc}

The CEO Shadow Program at GitLab is not a job title but a temporary assignment to shadow the CEO.
The shadows will be present at all meetings of the CEO. Like all meetings at GitLab, meetings will begin promptly regardless of shadow(s) attendance.
GitLab is [all remote](/company/culture/all-remote/) but the CEO has in-person meetings with external organizations. Therefore, you will stay in San Francisco during the entire [rotation](#rotation-rhythm) and travel with the CEO.

## Goal

The goal of the shadow program is to give current and future [directors and senior leaders](/company/team/structure/) at GitLab an overview of all aspects of the [company](/company/).
This should enable leadership to better perform [global optimizations](/handbook/values/#global-optimization).
Achieving this overview will come from context gained in [meetings attended](#meetings-and-events) and [learning while performing](/handbook/values/#bias-for-action) short-term [tasks](#tasks) from across the company.
The program will also create opportunities for the CEO to develop relationships with team members across the company and to identify challenges and opportunities earlier.
As an additional benefit, Shadows will often connect with one another, developing cross-functional relationships as a positive externality of the Program.

### Benefits for the company

Apart from creating leadership opportunities, the CEO shadow program:
* leaves a great impression on both investors and customers
* gives feedback immediately to the CEO
* enables the CEO to drive immediate change

It is because of these additional benefits that the program is worth the extra overhead on the CEO and [EBA team](/handbook/eba/).

### Naming

For now this role is called a [CEO shadow](https://feld.com/archives/2015/03/ceo-shadowing.html) to make it clear to external people why a shadow is in a meeting.

Other names considered are:

1. Technical assistant. Seems confusing with [executive assistant](/job-families/people-ops/executive-business-administrator/). ["In 2003, Mr. Bezos picked Mr. Jassy to be his technical assistant, a role that entailed shadowing the Amazon CEO in all of his weekly meetings and acting as a kind of chief of staff. "](https://www.theinformation.com/articles/amazons-cloud-king-inside-the-world-of-andy-jassy).
1. Chief of Staff. This commonly is the ["coordinator of the supporting staff"](https://en.wikipedia.org/wiki/Chief_of_staff) which is not the case for this role since people rotate out of it frequently. The executive assistants report to peopleops.
1. [Global Leadership Shadow Program](https://www2.deloitte.com/gr/en/pages/careers/articles/leadership-shadow-program.html) is too long if only the CEO is shadowed.

## Why and how to join (Participating)

### Reasons to participate

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/Cg0LzET_NWo" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

### Eligibility

You are eligible to apply for the program if you are currently or have accepted an offer at GitLab as a:

1. [Director or up](/company/team/structure/#layers), [Distinguished engineer or up](/job-families/engineering/backend-engineer/),  [Senior Product Manager or up](/job-families/product/product-manager) or [Manager](https://about.gitlab.com/company/team/structure/#manager) in the People Group
1. [Manager](/company/team/structure/#layers), [Staff engineer](/job-families/engineering/backend-engineer/), [SAL](/job-families/sales/strategic-account-leader/), [PMM](/job-families/marketing/product-marketing-manager/), or [Product Manager or up](/job-families/product/product-manager/), if there is 1 consideration.
1. [Individual Contributor](/company/team/structure/#layers), if there are 2 considerations.

Considerations are cumulative and can be:

1. You belong to an underrepresented group as defined in our [referral bonus program](/handbook/incentives/#referral-bonuses).
1. You are a recipient of GitLab’s Value Award of Transparency, Collaboration, Iteration, Efficiency, Results or Diversity at previous GitLab Contribute events.
1. There is a last minute cancellation and you respond quickly.
1. You are based out of APAC. 

You're also eligible if you work(ed) as an investor or helped with an investment in GitLab in a private round.

Being a CEO shadow is not needed to get a promotion or a raise and should not be a consideration factor for a promotion or raise, as diverse applicants have different eligibilities.

### How to apply

1. Create a merge request to add yourself to the [rotation schedule](#rotation-schedule). Ensure the merge request description highlights how you meet the eligibility criteria.
1. Ask your manager to approve (but not merge) the merge request. Managers, please ensure the candidate meets the eligibility criteria.
1. Assign the merge request to the CEO, link it in the #ceo-shadow channel, and @mention the CEO and the Executive Business Admin supporting the CEO in the message.

Please keep in mind when selecting dates that the CEO's schedule is fluid and subject to constant change, which in turn means that the CEO shadow rotation is also subject to constant change. The posted dates are not guaranteed. We will work with you to reschedule your rotation if a conflict should arise. 

### Rotation rhythm

We want many people to be able to benefit from this program, therefore we rotate often.
It is important that an incoming person is trained so that the management overhead can be light.
Currently, a rotation is two weeks:

1. See one, you are trained by the outgoing person.
1. Teach one, you train the incoming person.

The shadow should be available for the full two weeks.

When the CEO has a week or more of paid time off or during [Contribute](/events/gitlab-contribute/) the shadow program will pause, one shadow will "see one" before the break and "teach one" after the break.
The rotations with breaks of one or more weeks without a shadow are great if you can't be away from home for more than one week at a time.

If you need childcare to be able to participate, GitLab will [reimburse you](/handbook/spending-company-money/) for it.

This program is not limited just to long-term GitLab team members.
For new team members, this might be the first thing they do after completing our [onboarding](/handbook/general-onboarding/).
Exceptional community members may be able to participate, as well.

### Rotation schedule

| Start date | End date | See one | Teach one |
| ------ | ------ | ------ | ------ |
| 2020-01-13 | 2020-01-17 | [Keanon O'Keefe](https://gitlab.com/kokeefe) - Senior Product Manager | [Dov Hershkovitch](https://gitlab.com/dhershkovitch) - Senior Product Manager |
| 2020-01-20 | 2020-01-24 | [Dylan Griffith](https://gitlab.com/DylanGriffith) - Staff Backend Engineer | [Keanon O'Keefe](https://gitlab.com/kokeefe) - Senior Product Manager |
| 2020-01-27 | 2020-01-31 | [Brittany Rohde](https://gitlab.com/brittanyr) - Manager, Compensation & Benefits | [Dylan Griffith](https://gitlab.com/DylanGriffith) - Staff Backend Engineer |
| 2020-02-03 | 2020-02-07 | [Nadia Vatalidis](https://gitlab.com/Vatalidis) - Manager, People Operations |  [Brittany Rohde](https://gitlab.com/brittanyr) - Manager, Compensation & Benefits |
| 2020-02-10 | 2020-02-14 | [James Ramsay](https://gitlab.com/jramsay) - Sr. Product Manager | [Nadia Vatalidis](https://gitlab.com/Vatalidis) - Manager, People Operations |
| 2020-02-17 | 2020-02-21 | [Chase Wright](https://gitlab.com/wwright) - Manager, FP&A | [James Ramsay](https://gitlab.com/jramsay) - Sr. Product Manager |
| 2020-02-24 | 2020-02-28 | [Sarah Waldner](https://gitlab.com/sarahwaldner) - Product Manager | [Chase Wright](https://gitlab.com/wwright) - Manager, FP&A |
| 2020-03-02 | 2020-03-06 | [Shaun McCann](https://gitlab.com/shaunmccann) - Support Engineering Manager | [Sarah Waldner](https://gitlab.com/sarahwaldner) - Product Manager |
| 2020-03-09 | 2020-03-13 | AVAILABLE  | [Shaun McCann](https://gitlab.com/shaunmccann) - Support Engineering Manager |
| 2020-03-16 | 2020-03-20 | AVAILABLE  | AVAILABLE |
| 2020-03-23 | 2020-03-27 | NO SHADOWS - CONTRIBUTE  | NO SHADOWS - CONTRIBUTE |
| 2020-03-30 | 2020-04-03 | AVAILABLE  | AVAILABLE |
| 2020-04-06 | 2020-04-17 | NO SHADOWS - PERSONAL  | NO SHADOWS - PERSONAL  |
| 2020-04-20 | 2020-04-24 | [David DeSanto](https://gitlab.com/ddesanto) - Director of Product, Secure & Defend | AVAILABLE |
| 2020-04-27 | 2020-05-01 | AVAILABLE  | [David DeSanto](https://gitlab.com/ddesanto) - Director of Product, Secure & Defend |


If you have questions regarding the planned rotation schedule, please ping the Executive Admin to the CEO. The EBA to the CEO manages the rotation schedule, please do not add new dates to the schedule when adding in your rotation.

## What to expect (Preparation)

### Coordinating with your Co-Shadow

#### Coffee Chat
{:.no_toc}

Before your scheduled rotation, try to schedule coffee chats with your co-shadows before you start the program. This will give you the opportunity to get to know them and help set goals for the rotation.

#### Onboarding issue
{:.no_toc}

Outgoing shadows are responsible for training incoming shadows. We currently track on-boarding in the [ceo-shadow](https://gitlab.com/gitlab-com/ceo-shadow) project.
The incoming shadow is responsible to create an issue using the `on-boarding` template. Assign both incoming and outgoing shadows to the issue.

### Things to Know

1.  This isn't a performance evaluation.
1.  You **do not need to dress formally**; business casual clothes are appropriate. For example, Sid wears a button up with jeans most days. GitLab shirts are acceptable when there aren't any external meetings.
     - Review Sid's calendar and be appropriately prepared if there is a formal occasions
     - If unsure, please ask the Executive Business Administrator (EBA) in the `#ceo-shadow` slack channel
1.  Bring comfortable shoes with you to Mission Control any time there are meetings in the city. Heels are not a good idea. Sid prefers to walk, even if his calendar says Uber.
1.  Review the CEO's calendar to get an idea of what your upcoming weeks will be like.
1.  Review and update the ongoing [CEO Shadow tasks project](https://gitlab.com/gitlab-com/ceo-shadow/tasks/-/boards/1385894). This project contains TODOs, documentation items, training, and feedback information item and is in the CEO Shadow channel description on Slack.
1.  Plan to observe and ask questions.
1.  Give feedback to and receive feedback from the CEO.
1.  Don't plan to do any of your usual work. Prepare your team as if you were on vacation.
1.  Be ready to add a number of [handbook](/handbook/handbook-usage/) updates during your shadow period.

### Meeting attendance

You will attend all meetings of the CEO, including but not limited to:

1. 1-1s with reports.
1. Interviews with applicants.
1. Conversations with board members.

You will travel with the CEO to meetings, team off-sites, and conferences outside of San Francisco per the CEO's schedule.
Executive Business Admin to the CEO will assist you with conference registration and travel accommodations during these time frames.

The CEO's Executive Business Admin will ask external people if they are comfortable with the Shadow joining prior to the scheduled meeting and will share a link to the CEO Shadow page to provide context.

Meeting agendas should be shared with `ceo-shadow@gitlab.com`, as shadows will be added to this email alias prior to the rotation and removed at the conclusion of it.
For agendas that contain sensitive information, the sensitive information should be removed and the document shared with "View only" access to restrict access to the document's history.
Not all agendas will be shared, though, and the CEO Shadows should feel empowered to ask for access if that is the case.
Sometimes the answer will be no for sensitive reasons.

These meetings can have different formats:

1. Video calls.
1. In-person meetings.
1. Dinners that are business related.
1. Customer visits.
1. Conferences.

You will not attend a meeting when:

1. Someone wants to discuss a complaint and wants to stay anonymous.
1. If any participant in the meeting is uncomfortable.
1. If the CEO wants more privacy.

The CEO may occasionally invite you to optional meetings that may not be explicitly GitLab related but can help provide insight into his day to day activities. In these activities, it is asked that you not take notes because we don't want you to do work that isn't for GitLab. Also these meetings are optional and you can leave at any time.

This is probably the most open program in the world.
It depends on the participants respecting confidentiality, during the program, after the program, and after they leave GitLab.

## What does a shadow do? (Execution)

### Tasks

The value of the CEO shadow program is obtained via the [broader context](#goal) you will derive and the interesting conversations you will witness.

Since a rotation is over a short period there are no long running tasks that you can assume.
However, there are many short-term administrative tasks you will also be asked to perform, for example:

1. Make [handbook](/handbook/) updates (use the [ceo-shadow](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests?scope=all&utf8=%E2%9C%93&state=all&label_name[]=ceo-shadow) label).
   Post the MR links in the #ceo-shadow Slack channel so the CEO knows they have been completed.
1. Publicly advise when folks are not following the [communication guidelines](/handbook/communication/). ([See more details below.](https://about.gitlab.com/handbook/ceo/shadow/#promote-communication-best-practices))
1. Iterate and complete small tasks as they come up. Clear them out immediately to allow for rapid iteration on more crucial tasks. Communicate updates on those tasks in the #ceo-shadow channel.
1. Solve urgent issues, for example a complaint from a customer or coordinating the response to a technical issue.
1. Prepare for, take notes during and follow up on meetings. ([See more details below.](https://about.gitlab.com/handbook/ceo/shadow/#taking-notes-in-meetings))
1. Share [thanks](/handbook/communication/#say-thanks) in the `#thanks` channel in Slack when it comes from a customer or wider community member in a meeting.
1. Compile a report on a subject.
1. Write an [Unfiltered blog post](/blog/categories/unfiltered/) based on a conversation, something you learned, or your experience. These do not need to be approved by the CEO but he will happily review them if you'd like. Please see [the blog handbook](/handbook/marketing/blog/unfiltered/) for information about the publishing process, and be sure to read previous CEO shadows' blog posts before you start writing to ensure that your post has a new angle.
1. Ensure visual aids and presentations are visible to guests during in-person meetings.
1. Control slides and muting during the board meeting.
1. Prepare for and receive guests at Mission Control.
1. Offer GitLab swag to guests at Mission Control before they leave.
1. Answer the phone and door at Mission control.
1. Do and publish a [CEO interview](https://gitlab.com/gitlab-com/www-gitlab-com/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=CEO%20interview).
1. Improve and provide training to incoming CEO Shadows.
1. Speak up when the [CEO displays flawed behavior](/handbook/ceo/#flaws).

#### Collecting and managing tasks
{:.no_toc}

CEO Shadows maintain a project called [CEO Shadow Tasks](https://gitlab.com/gitlab-com/ceo-shadow/tasks/-/boards/1385894).
It is linked in the #ceo-shadow Slack channel description.
Collect tasks using the first name of the shadow who captured it.
Add at the end to indicate who will action it.
Once those items have an open MR against them, post in the #ceo-shadow channel.

#### Attending Meetings with the CEO
{:.no_toc}

1. At the start of the week, review the CEO's calendar. The CEO's calendar is the single source of truth. Shadows should check the CEO calendar for updates often. You will not be invited to each meeting, instead meetings that the shadows may not attend will have a separate calendar entry on the CEO's schedule that states "No CEO Shadows". When in doubt, reach out to CEO Executive Business Admin to confirm if you should attend or not. There will be some meetings and events the Shadows do not attend.
1. Add the CEO's calendar to your Google Calendar by clicking the `+` next to "Other Calendars".  Then click `Subscribe to Calendar`, search for the CEO and click enter.
1. Because interviews are marked private (busy) for confidentiality reasons the Executive Assistant will invite Shadows to those events directly. As a result, you will get an email from Greenhouse asking for candidate feedback, which is not necessary.
1. Shadows are responsible for being aware of the current time and providing a verbal 5 minute warning to the meeting participants so that they can comfortably wrap up the meeting. Don't wait minutes for a break, just say 'we have 5 minutes left'. Please use the calendar invite as an indication how long the meeting should last, we do speedy meetings.
1. Shadows should notify meeting participants if a meeting is running over the allocated time by saying 'we're in overtime'.
1. Shadows should notify Gitlab meeting participants if their [name and job title](/handbook/tools-and-tips/#adding-your-title-to-your-name) are not mention on Zoom.

#### Taking Notes in Meetings
{:.no_toc}

You should always assume that you are taking notes in a meeting unless it is a 1:1 between the CEO and his direct reports or it is explicitly stated not to. If unsure whether or not to take notes, default to taking them or ask the CEO. 
It is better to have too many notes, than not enough.  If there is not already a notes document, see the [templates available here](https://about.gitlab.com/handbook/eba/#meeting-request-requirements).

#### Promote Communication Best Practices
{:.no_toc}

It's important that everyone encourages others to follow the [communication guidelines](/handbook/communication/), not just the CEO. As Shadows, in Group Conversations and other settings, you should remind team members to:
*  Verbalize questions
*  Stop sharing their screens to encourage conversations
*  Provide full context for the benefit of new team members

#### Friendly competition
{:.no_toc}

CEO Shadows label the handbook MRs they create with the `ceo-shadow` label.
It's a point of competition between CEO Shadows to try to best the previous shadows number of merge requests.

<iframe class="dashboard-embed" src="https://app.periscopedata.com/shared/40ce74a5-00ff-4a5d-8162-b1fe212d8cbc?embed=true" height="700"> </iframe>

### Email Best Practices
In order to ensure continuity across CEO shadow participants. Always, cc `ceo-shadow@gitlab.com` on emails as part of the program. This ensure that even after
you've left the program the response and follow up can be tracked.

### Follow activity from the CEO
Shadows are encouraged to follow the CEO's activity on various platforms to gather a complete picture of his everyday engagements and to get additional details on where he directs his attention.

### In Slack
{:.no_toc}
Go to the Slack search bar and type "from:@sid" and it will populate the results.

![Slack User Activity](/images/ceoshadow/slackuseractivity.png){: .shadow.medium.center}
Follow Sid's Slack activity to follow his everyday engagements
{: .note.text-center}

### In GitLab
{:.no_toc}
This can be seen on the CEO's [GitLab activity log](https://gitlab.com/users/sytses/activity).

![GitLab Activity Log](/images/ceoshadow/gitlabactivitylog.png){: .shadow.medium.center}
See what issues and MRs Sid is interacting with
{: .note.text-center}

### On Twitter
{:.no_toc}
Go to [Sid's twitter account](https://twitter.com/sytses).

![Twitter notification](/images/ceoshadow/twitternotification.png){: .shadow.medium.center}
Sign up for twitter notifications (twitter account required) to follow his everyday engagements.
{: .note.text-center}

### Documentation focus

An ongoing shadow program with a fast rotation is much more time consuming for the CEO than a temporary program or a rotation of a year or longer.
Therefore most organizations either have a shadow for two days or have someone for a year or more.
We want to give many people the opportunity to be a shadow so we rotate quickly.
To make this happen without having to invest a lot of time to train people coming in we need great documentation.
Therefore a quick turnaround on documentation is of paramount importance.
And the documentation will have a level of detail that isn't needed in other parts of the organization.

### CEO shadow introductions

When introducing yourself in a meeting as the first shadow say:

- I'm NAME.
- I normally am the TITLE.
- This is my first/last week in the two weeks CEO shadow program.
- The goal of the program is to give participants an overview of the functions at GitLab.

When introducing yourself in a meeting as the second shadow say:

- I'm NAME.
- I normally am the TITLE.
- This is my first/last week in the CEO shadow program.

### Meetings and Events
Meetings come in many different formats, and your responsibilities will change based on the kind of meeting.

In video calls, speak up when the CEO's camera isn't working or when the green screen isn't working correctly because of the sun angle.

![Sun on Green Screen Zoom Issue](/images/ceoshadow/ceo_sun_zoom_issue.png){: .shadow.medium.center}

#### Company Call
{:.no_toc}

CEO Shadows should attend the company call if the CEO does.
When it comes time for the [breakout call](/handbook/communication/#breakout-call), CEO Shadows should attend their own breakout calls.
You are encouraged to share your experience as a shadow with your breakout call groups while you are shadowing.

#### Participating in Media Briefings
{:.no_toc}

CEO Shadows may be the point of contact for helping coordinate (not schedule) media briefings.
Take initiative, for example finding a quiet space for the CEO to take the call, if it is done while traveling.
When participating in media briefings, CEO Shadows are to act as silent participants, except when directly asked a question.

#### Attending in-person events with the CEO
{:.no_toc}

When attending events with the CEO, keep the following in mind:
1. Remind the CEO to bring extra business cards before leaving. And bring a few for yourself.
1. The CEO has outlined his [transport preferences](/handbook/ceo/#transport).
1. When traveling to events in the Bay area by car, the CEO will request the ride sharing service. 
1. When traveling to events on foot, CEO Shadows should take responsibility for navigating to the event.
1. After a talk or panel, be ready to help the CEO navigate the room, particularly if there is a time sensitive obligation after the event.

The CEO often has work events that are also social events.
In Silicon Valley, social and work are very intertwined.
These mostly take the form of lunches or dinners.
CEO shadows are invited unless otherwise specified, but there is no expectation or obligation to join, this is an optional part of the program.

#### Attending candidate interviews with the CEO
{:.no_toc}

If the candidates consent, CEO Shadows will attend interviews performed by the CEO.
When scheduling an interview with the CEO, the EBA to CEO will create a shared GoogleDoc for notes between the shadows and the CEO. The doc template can be found by searching "Notes Doc for Candidate Interviews" in Google Drive. If you have any questions, please @ mention the EBA to CEO in #ceo-shadow in slack
This notes document is then added to the Scorecard for the candidate in GreenHouse.
Shadows should ensure they mark comments they provide with their full name.

#### Traveling with the CEO
{:.no_toc}

When traveling with the CEO, keep the following in mind:
1. Book flights that will allow you to land before the CEO so there is no delay in transportation to the next event.
1. For Airport pickup with the CEO, research the terminal the CEO arrives in and plan to be there to meet with the driver before the CEO.
1. Keep the EBA to CEO and onsite EBA updated regularly and promptly on estimated time arrival in #ceo-shadow slack channel to ensure the schedule is on-time.
1. If travel plans change, please update the EBA(s) in slack immediately so cancellations to prior transportation can be made promptly to not incur fees.
1. When returning to San Francisco if on a different airline, be sure to arrive before CEO and communicate [meetup location if traveling back to Mission Control](#rideshare-from-airport) together.

## Mission Control Guide

### Working from Mission Control

You are welcome to work from Mission Control but it is not required to be present in-person unless there is an in-person meeting, event, or dinner. It's up to you to manage your schedule and get to places on time. If you are traveling somewhere, meet the CEO at Mission Control at the beginning of the allotted travel time listed on the calendar.

If there is a day during your program where all meetings are Zoom meetings, you can work from wherever you want, as your normally would.
You can work from Mission Control if you prefer.
If you decide to split your day between remote work and working from Mission Control, make sure you give yourself enough time to get to Mission Control and set up for the guest.
It's OK to join calls while mobile.
In addition, feel free to coordinate or join a co-working day with GitLabbers in the Bay Area.
To coordinate join the #loc_bayarea Slack channel.

Shadows are welcome at Mission Control 10 minutes prior to the first meeting until 6pm. Feel free to ask if you can stay later. Don't worry about overstaying your welcome, if Karen or Sid would like privacy they will ask you to leave explicitly.

One more thing: the cat feeder is automatic and goes off daily at 10:22am PT (as well as another time, it's a surprise!). No need to be alarmed by the metallic clanging sound.

### Working outside Mission Control

Outside of Mission Control hours, shadows have the following options:

* If not an in person meeting, you are welcome to take the meeting from your lodging and then proceed to Mission Control during the Group Conversation or Company call.
* There are coffee shops with early opening hours with Wifi access near Mission Control. This is a great venue to meet with your co-shadow if coordination is needed before heading in.
  * [Philz Coffee on Folsom Street](https://www.google.com/maps/place/Philz+Coffee/@37.7890402,-122.3975675,16z/data=!4m8!1m2!2m1!1sphilz+coffee!3m4!1s0x0:0xba68b065a8d2c93b!8m2!3d37.7887131!4d-122.3930718), opens at 05:30am.
  * [Philz Coffee on Front Street](https://www.google.com/maps/place/Philz+Coffee/@37.7890402,-122.3975675,16z/data=!4m8!1m2!2m1!1sphilz+coffee!3m4!1s0x0:0x4b3009758e346914!8m2!3d37.7917908!4d-122.3990035), opens at 05:30am.
  * [Starbucks Reserve on Mission St.](https://www.google.com/maps/place/Starbucks+Reserve/@37.7896722,-122.3933832,17z/data=!4m8!1m2!2m1!1sstarbucks!3m4!1s0x0:0x76ac8e682da13edb!8m2!3d37.791511!4d-122.3951029), opens at 05:00am.
  * [Starbucks on the 5th floor of the Salesforce tower](https://www.google.com/maps/place/Starbucks/@37.7899796,-122.3966609,18z/data=!4m8!1m2!2m1!1sstarbucks!3m4!1s0x0:0x4e742c0956f70966!8m2!3d37.7899051!4d-122.394609), opens at 04:00am.

### Mission Control access

When entering the building, the doorperson may ask who you are there to see. Don't say "GitLab" since there is no GitLab office. The doorperson will direct you to the correct lobby.

While there are two sets of keys, it's worthwhile coordinating access to Mission Control with the outbound shadow on your first day. Meeting up on Sunday evening or, at a specific time on Monday morning. This will enable the incoming shadow to be introduced into Mission Control without impacting Sid and/or Karen.

### Mission Control device setup

#### Configuring the screens
{:.no_toc}

We have six monitors at Mission Control. They should be configured as follows:

|:---:|:---:|:---:|
|**Top Left**<br/>[Team](/company/team)|**Top Middle**<br/>[Category Maturity](/direction/maturity/)|**Top Right**<br/>[Clari](https://app.clari.com) Sales Dashboard - This Quarter|
|**Bottom Left**<br/>[Category Maturity](/direction/maturity/) | **Bottom Middle**<br/>[Who we replace](/devops-tools/)| **Bottom Right**<br/>[Clari](https://app.clari.com) Sales Dashboard - Next Quarter |

Turning all screens on or off might be challenging since a single remote controls all of them, the easiest way to do that is by covering the tip of the remote with your hand and getting as close as possible to a single screen while turning it on or off.

To configure the sales dashboards:

1. Go to [Clari](https://app.clari.com).
1. Go To Pulse tab.
1. Open the left side bar.
1. Click on the funnel icon. Select “CRO”.
1. Click on the gear icon. Go to Forecasting. Select Net IACV.

#### How to use keyboard and mouse to update screens
{:.no_toc}

The wireless mouse and keyboard are connected to the bottom left TV by default because that one is visible from both sides of the conference table. To update the view on another TV, you have to connect the wireless keyboard and mouse to the desired screen. Afterwards don't forget to return it to the bottom left position for use during meetings.

1. Find the USB jacks and Logitech receiver underneath the bottom, right TV (they're all labeled).
1. Connect the Logitech receiver to USB receiver for the desired screen.
1. To log into the chrome devices at Mission Control, use the login information in the "[CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi)" in 1Password.

#### Troubleshooting the TV Screens

Turning all screens on or off might be challanging since a single remote controls all of them, the easiest way to do that is by covering the tip of the remote with your hand and getting as close as possible to a single screen while turning it on or off.

Each of the screens at Mission Control use an Asus Chromebit to display the preferred content and are connected to the HDMI1 port of each TV. If you turn on the TVs and one displays a blank screen while on the HDMI1 input, the Chromebit may need to be reset. You can do this by power cycling the Chromebit (they are located behind the bottom right TV and and are labeled) connecting the Chromebit to the [Keyboard and Mouse](https://about.gitlab.com/handbook/ceo/shadow/#how-to-use-keyboard-and-mouse-to-update-screens) and logging into it using the credentials in the "[CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi)" via 1Password. Once you have logged into the Chromecst you can quickly find the proper content as listed in [Configuring the Screens](https://about.gitlab.com/handbook/ceo/shadow/#configuring-the-screens) via the browsers recent history.

#### AirPlay
{:.no_toc}

To screencast from an iPad or MacBook to the top left screen, switch the "Source" on the **top left** screen to "Apple TV" (HDMI 2).

Using the larger remote (with the white buttons), you can press the white, center button in the top row of buttons; this will bring up a list of sources. There is a direction pad on the remote towards the bottom that has `< ^ > v` buttons as well as the selection button in the center.

When the TV is on the Apple TV source you may need to change the Apple TV to
AirPlay mode. The Apple TV remote is the small black one with only 5 buttons.
You can click "MENU" until it says AirPlay.

#### Printer
{:.no_toc}

The printer at Mission Control is called `HP Officejet Pro 8610` and is available over AirPlay/Wifi. The printer is located in Sid's office.

#### Zoom Room
{:.no_toc}

Zoom Rooms is an application used for team members not in San Francisco to participate in meetings happening at Mission Control. There's a separate screen (the **large** one on wheels), a Mac Mini and, iPad at Mission Control for this purpose. The Mac Mini is connected to HDMI1 on the screen and, the iPad operates as a remote control for Zoom Rooms.

1. **Setup**
    1. Turn on the big screen on wheels.
    1. Turn on the Mac Mini.
    1. Start the Zoom Rooms application on the Mac Mini.
    1. If you have to log in or, provide a passcode, both are in the [CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi).
    1. Start the Zoom Rooms application on the iPad (you may need the passcode).
    1. If there's any problem connecting to the service, log out and back in. If that fails, contact the Executive Admin to the CEO.
1. **If you do not have the option to join a meeting on the iPad (It's in "Scheduling mode" - you don't have a "Meet Now" or "Join" button):**
    1. Make sure Zoom Rooms on the iPad is logged in.
    1. Click the settings "gear" icon in the top right hand corner.
    1. Disable the "lock settings" option (you'll need the passcode).
    1. Tap on "Zoom Room Mission Control".
    1. Tap "Switch to Controller".
1. **If the Zoom Rooms app on the iPad is not connecting:**
    1. Check the credentials for the Zoom Room in the [CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi). If unable to log in, please contact the Executive Admin to the CEO on slack.
    1. Log out of the Zoom Rooms app on the iPad.
    1. Log in using the PeopleOps Zoom account credentials in the [CEO Shadow Vault](https://gitlab.1password.com/vaults/tq7hndbk2xjcwc5czwcq7qt6kq/allitems/piqiei3hcjdz7lpmpsyppwxmfi).

The CEO Shadow is often responsible for handling the technical details of coordinating Zoom meetings. If using a **webinar**, you will need to be a co-host in order to promote folks to panelists for them to be able to verbalize their own questions. 

#### Maintaining Software and Hardware

The devices in mission control are configured to auto-update when new versions are available. As a precaution, Shadows in their second week should confirm all software in Mission Control is update to date.

### Updating the TV's Software
{:.no_toc}

To check that a Samsung TV is up to date:

1. Grab the remote labled for the TV
1. Hit the home button
1. Go to "Settings"
1. Go to "Support"
1. Select "Software Update"
1. Select "Update Now"

### Visitor prep

In preparation for guests (customers, investors, etc.) who will be meeting with the CEO or other team members at Mission Control, please note the following prior to the meeting start:

1. All GitLab team-members sit on one side. This allows for easy communication with the guests.
1. Set the lighting mode to 'Evening' - lighting controls are located next to the kitchen entrance.
1. Have drinks from the fridge (on the tray) available on the table.
1. Get the glasses from the cupboard above the coffee machine and place them next to the drink tray.
1. Place the keyboard and the mouse (used for Mission Control screens) on the table.
1. For investor or banker meetings, see the [confidential issue](https://gitlab.com/gitlab-com/ceo-shadow/tasks/issues/50) with more details on preparations.
1. If someone is attending a meeting at Mission Control via [Zoom Rooms](#zoom-room):
    1. Follow the setup steps for [Zoom Rooms](#zoom-room).
    1. Move the screen to the head of the table.
    1. Click 'Join' in the Zoom Rooms menu and enter the meeting ID from the Google Calendar invite of the meeting in question.
    1. Once the meeting is loaded, click on the participants list and make sure that the iPad is visible from the table.

### Welcoming Visitors to Mission Control

1. The front desk will call the apartment when a visitor arrives.
1. A Shadow should meet guests at the elevator, greet them, and guide them to Mission Control.
1. Once the meeting is over, a Shadow should escort the guests back to the elevator.

### Mission Control FAQs

1. Everything in the fridge that is liquid can be consumed including Soylent and alcohol.
    1. If the beverages are running low, check the top cupboard above the oven (next to the fridge).
    1. There's a step ladder in the laundry (last door on the **left** down the hall way)
    1. If the cupboard is also running low, reach out to the Executive Admin to the CEO with the list for restocking.
1. Thermometer is located next to the kitchen entrance.
1. Lighting controls are located next to the kitchen entrance, select 'Evening'.
1. Coffee machine is located in the kitchen, coffee pods are in the drawer below the coffee machine.
1. Swag (socks and stickers) is located on the bookshelf near the dashboards.  
    1. If the swag is running low, reach out to the Executive Admin to the CEO with a list for restocking
1. When joining a podcast in Zencaster, a microphone error can be avoided by leaving your microphone unmuted for the first 30 seconds / minute.
    1. Zencaster checks that your mic is working by recording audio so, muting the mic causes the error.
    1. The system check happens when you first load the page
    1. If you get the microphone error, reload the page and wait for the checks to finish before muting.

## Expenses, travel and lodging

### Lodging
Lodging during the CEO shadow program is provided by the company. Executive Admin to the CEO books the accommodation based on availability and cost. You can express your preference (hotel or AirBnB) via email to the Executive Admin to the CEO in question, however the final decision is made by the Executive Admin based on the distance from CEO and costs. Executive Admin will provide the accommodation details no earlier than 1 month and no later than 2 weeks before the scheduled rotation.

Accommodation is provided only for the active shadowing period, it is not provided during the shadow program pause (cases when the CEO is unavailable).
In case you are coming from a timezone that is more than 6 hours difference with Pacific Time, it is possible to book the weekend before the first shadow work day to adjust to the new timezone.

If your CEO shadow rotation is two consecutive weeks, it is expected you will be staying the weekend. Accommodation is provided during the weekend.

### Airfare
Airfare can be booked according to our [travel policy](/handbook/travel/#booking-travel-and-lodging) or [spending company money](/handbook/spending-company-money/) policy.
In case your shadow rotation includes time without shadowing, it is possible to expense airfare to fly home and back within the continental USA. If you are from outside of the USA, it is also possible to expense airfaire during the time without shadow because of the possible high cost of lodging in San Francisco if you chose to stay at a different location.

#### Rideshare from airport  

At San Francisco International airport (SFO), all rideshare apps (Uber, Lyft, etc) pick up on level 5 of the parking structure. When coordinating travel from SFO to Mission Control with other Shadows, GitLabbers or Sid arranging to meet on level 5 of the parking structure is most efficient as each terminal has its own baggage claim area. 


### Childcare
Childcare is provided during the active shadowing period and will be reimbursed via your expense report. You must book the childcare yourself and it is advised you reach out far in advance as childcare "drop-ins" can be limited depending on the week. Currently, GitLab doesn't have a ["Backup Care"](https://www.brighthorizons.com/family-solutions/back-up-care) program so you must tell the childcare it is for a "drop-in".  Depending on your hotel accommodations, finding a nearby daycare is most convenient or a daycare nearby the [Millennium tower](https://www.google.com/maps/place/Millennium+Tower+San+Francisco/@37.7905055,-122.3962516,15z/data=!4m2!3m1!1s0x0:0x9fe15ebd4a8300d8?sa=X&ved=2ahUKEwiUoZ_hpb_iAhXBop4KHeOAB2QQ_BIwGHoECAsQCA). Some childcare facilities will require payment at end-of-day or end-of-week via cash/check only so request an invoice/receipt for expense submission purposes.

Past Childcare facilities that have been accommodating:

1. [Bright Horizons at 2nd Street](https://child-care-preschool.brighthorizons.com/ca/sanfrancisco/2ndstreet?utm_source=GMB_yext&utm_medium=GMBdirectory&utm_campaign=yext&IMS_SOURCE_SPECIFY=GMB) - This facility is nearest the [Courtyard by Marriott SF Downtown Hotel](https://www.google.com/maps/place/Courtyard+by+Marriott+San+Francisco+Downtown/@37.785751,-122.3997608,16.7z/data=!4m12!1m6!3m5!1s0x8085807c643d1007:0x85815e04bf8d233c!2sCourtyard+by+Marriott+San+Francisco+Downtown!8m2!3d37.7859011!4d-122.3969222!3m4!1s0x8085807c643d1007:0x85815e04bf8d233c!8m2!3d37.7859011!4d-122.3969222).
    * Contact: [Rose - Current Director](https://child-care-preschool.brighthorizons.com/ca/sanfrancisco/2ndstreet)


## Parental Participation in the CEO Shadow program
We understand that participation in the CEO Shadow program is optional and can cause hardships at home. To help overcome these challenges and to allow flexibility for parents to participate, there will be some weeks identified as "parent-friendly". These are weeks where Sid doesn't need a shadow for the full 5 workdays or where the program is split so the weeks are not consecutive. 


## Suzy (the cat)

Please note that we have a cat named [Suzy](/company/team-pets/#7-suzy). It is a Russian Blue mix which is a [hypoallergenic variety](https://www.russianbluelove.com/russian-blue-cat-allergies/). If you're allergic to cats consider washing your hands after petting.

### Petting
{:.no_toc}

Suzy likes attention and will invite you to pet her. Please don't pet her after/when she meows since that reinforces the meowing which can be annoying during calls and the night. You can pick her up but she doesn't like it much and will jump out after about 30 seconds.

#### Hypoallergenic Petting (Paddle Petting)
{:.no_toc}

If you are allergic to cats and don't want to wash your hands everytime after petting Suzy you can *gently* paddle pet her using ping pong paddles available in Mission Control.

![](/images/handbook/suzypetpillow.jpg)

The white pillow on the sofa in Mission Control is the **only** place to paddle pet Suzy. She really enjoys it when you *gently pat* her sides with the ping pong paddles when she is **on** the white pillow, if she steps **off** the pillow stop petting her. When she gets back onto the pillow you can resume gently paddle petting her.

<!-- blank line -->
##### Paddle Pat technique with Eric Brinkman
{:.no_toc}
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/vf-uEPweMUg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

<!-- blank line -->
##### Paddle Pat & Rub technique with JJ Cordz
{:.no_toc}
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/KLWQH0EDtcg" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


## Recommended Food/Drinks nearby

### Food
{:.no_toc}

There is a [Yelp list](https://www.yelp.com/collection/sVnYVUU_npZzJ6h9koBbxw) that contains recommendations from previous Shadows, as well as the [CEO's Favorite Restaurants](/handbook/ceo/#favorite-restaurants).

The list is administered by the `ceo-shadow@` email address. Login to Yelp with the credentials in the CEO Shadow 1Password vault to add restaurants, update notes, or remove items from the collection.

### Drink
{:.no_toc}

1. [Bluestone Lane](https://www.google.com/maps/place/Bluestone+Lane/@37.7900021,-122.4031358,16.56z/data=!4m12!1m6!3m5!1s0x8085808814a10285:0x3b8f6e4330a367d9!2sBluestone+Lane!8m2!3d37.7878896!4d-122.4028954!3m4!1s0x8085808814a10285:0x3b8f6e4330a367d9!8m2!3d37.7878896!4d-122.4028954)
    * Tye Davis Favorite (AMAZING COFFEE) - long walk
1. [Starbucks](https://www.google.com/maps/place/Starbucks/@37.7899941,-122.3977364,18.43z/data=!4m12!1m6!3m5!1s0x8085808814a10285:0x3b8f6e4330a367d9!2sBluestone+Lane!8m2!3d37.7878896!4d-122.4028954!3m4!1s0x80858067244672f9:0xea9a5743328af8ff!8m2!3d37.789545!4d-122.397592)

### Rewards Cards ("punch cards")
{:.no_toc}

Occasionally food trucks or restaurants have loyalty rewards cards. It is **not required** but if you get one and want to leave it for future Shadows to use please add to this list and put the reward card in the CEO Shadow drawer at Mission Control.

1. [Bowld Acai](https://www.bowldacai.com/) Food Truck

## What is it like?

Prospective CEO shadows can read, listen and watch key takeaways from program alumni below.

1. [Day 2 of Erica Lindberg](https://www.youtube.com/watch?v=xrWR0uU4nbQ)
2. [Acquisitions, growth curves, and IPO strategies: A day at Khosla Ventures](/blog/2019/04/08/khosla-ventures-gitlab-meeting/)
3. [GitLab CEO Shadow  Update - May 30, 2019](https://www.youtube.com/embed/EfBMu9dTpno)

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/EfBMu9dTpno" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
4. [Key takeaways from CEO Shadow C Blake](https://youtu.be/3hel57Sa2EY)
 <!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/3hel57Sa2EY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->
5. AMA with the CEO Shadow Alumni on 2019-08-23
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/TxivABJ16jE" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Alumni

CEO Shadow program alumni are welcome to join the #ceo-shadow-alumni slack channel to stay in touch after the program.

| Start date | End date | Name | Title | Takeaways |
| ------ | ------ | ------ | ------ | ------ |
| 2019-03 | 2019-04 | [Erica Lindberg](https://gitlab.com/erica) | Manager, Content Marketing | [CEO shadow learnings video](https://www.youtube.com/watch?v=xrWR0uU4nbQ) |
| 2019-04 | 2019-05 | [Mayank Tahil](https://gitlab.com/mayanktahil) | Alliances Manager |
| 2019-04 | 2019-05 | [Tye Davis](https://gitlab.com/davistye) | Sr. Technical Marketing Manager | [Without a shadow of a doubt: Inside GitLab's CEO shadow program](/blog/2019/07/11/without-a-shadow-of-a-doubt/)
| 2019-05 | 2019-06 | [John Coghlan](https://gitlab.com/johncoghlan) | Evangelist Program Manager | [5 Things you might hear when meeting with GitLab's CEO](/blog/2019/06/28/five-things-you-hear-from-gitlab-ceo/)
| 2019-06 | 2019-06 | [Cindy Blake](https://gitlab.com/cblake) | Sr. Product Marketing Manager | [CEO shadow learnings video](https://www.youtube.com/watch?v=3hel57Sa2EY)
| 2019-06 | 2019-06 | [Nnamdi Iregbulem](https://gitlab.com/whoisnnamdi) | MBA Candidate at Stanford University |
| 2019-06 | 2019-06 | [Clinton Sprauve](https://gitlab.com/csprauve) | PMM, Competitive Intelligence |
| 2019-06 | 2019-07 | [Lyle Kozloff](https://gitlab.com/lkozloff-admin) | Support Engineering Manager |
| 2019-07 | 2019-07 | [Marin Jankovski](https://gitlab.com/marin) | Engineering Manager, Deliver |
| 2019-07 | 2019-08 | [Danae Villarreal](https://gitlab.com/DanaeciousV) | Sales Development Representative, West |
| 2019-08 | 2019-08 | [Daniel Croft](https://gitlab.com/dcroft) | Engineering Manager, Package | [GitLab, CEO Shadow August 2019 week one, mind blown](https://www.youtube.com/watch?v=VHcA_2UsC2k) |
| 2019-08 | 2019-08 | [Emilie Schario](https://gitlab.com/emilie) | Data Engineer, Analytics | [What I learned about our CEO's job from participating in the CEO Shadow Program](/blog/2019/10/07/what-i-learned-about-our-ceo-s-job-from-participating-in-the-ceo-shadow-program/) |
| 2019-08 | 2019-08 | [Kenny Johnston](https://gitlab.com/kencjohnston) | Director of Product, Ops |  |
| 2019-08 | 2019-09 | [JJ Cordz](https://gitlab.com/jjcordz) | Senior Marketing Operations Manager |  |
| 2019-09 | 2019-09 | [Eric Brinkman](https://gitlab.com/ebrinkman) | Director of Product, Dev |  |
| 2019-09 | 2019-10 | [Danielle Morrill](https://gitlab.com/dmor) | General Manager, Meltano |  |
| 2019-10 | 2019-10 | [Mek Stittri](https://gitlab.com/meks) | Director of Quality |  |
| 2019-10 | 2019-11 | [Kyla Gradin](https://gitlab.com/kyla) | Mid Market Account Executive |  |
| 2019-10 | 2019-11 | [Clement Ho](https://gitlab.com/ClemMakesApps) | Frontend Engineering Manager, Monitor:Health |  |
| 2019-11 | 2019-11 | [Brendan O'Leary](https://gitlab.com/brendan) | Sr. Solutions Manager |  |
| 2019-11 | 2019-11 | [Gabe Weaver](https://gitlab.com/gweaver) | Sr. Product Manager, Plan: Project Management |  |
| 2019-11 | 2020-01 | [Chenje Katanda](https://gitlab.com/ckatanda) | Technical Account Manager | |
| 2020-01 | 2020-01 | [Dov Hershkovitch](https://gitlab.com/dhershkovitch) | Senior Product Manager, Monitor |  |

