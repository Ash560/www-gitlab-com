---
layout: markdown_page
title: "Category Direction - Kanban Boards"
---

- TOC
{:toc}

## 🚥 Kanban Boards

|          |                                |
| -------- | ------------------------------ |
| Stage    | [Plan](/direction/plan/)       |
| Maturity | [Viable](/direction/maturity/) |

### Introduction and how you can help

<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category strategy page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This strategy is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->

👋 This is the category strategy for Kanban Boards (Issue Boards) in GitLab; which is part of the Plan stage's [Project Management](/handbook/categories/#project-management-group) group. Please reach out to the group's Product Manager, Gabe Weaver ([E-mail](mailto:gweaver@gitlab.com)), if you'd like to provide feedback or ask any questions related to this product category.

This strategy is a work in progress and everyone can contribute:

- Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Aproject%20management) and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AIssue%20Tracking) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
- Please share feedback directly via email, Twitter, or on a video call.

### Overview

<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->

#### Purpose

GitLab's mission is to build software so that **everyone can contribute**. Issue boards are intended to helps teams collaborate together more effectively during the planning and execution of sprints and releases.

#### Essential Intent

The goal of a Category's "Essential Intent" is to provide a concrete, inspirational statement. Another way to think of it is answering this single question -- *"If Issue Boards can be truly excellent at only one thing, what would it be?"* This is Issue Boards' Essential Intent:

> To provide **flow** for teams that empowers them to achieve their **highest point of contribution** towards their community's **purpose** by always working on the right thing, for the right reason, at the right time in a transparent, collaborative, and consistent manner.

Next, asking "How will we know when we're done?" provides clarity of purpose. This is how we will know:

- Teams can create workflows that automate the movement of issues between Issue List based on rules they define.
- Issue Boards can be configured to support Timeboxed or Continuous Planning models with the click of a button.
- Changes to data on reflected on issue boards is propogated to all connected clients and rendered in under a second.
- Teams can reliably communicate when any issue on an issue board will be completed with 99% accuracy.
- Team Members can immediately understand what next steps they need to take on which issues and in what order just by looking at the Issue Card.
- Issue Boards provide built in modes for various "Agile" practices such as backlog grooming or conducting release planning.

#### How we prioritize

We use the following decision framework when evaluating what to work on. Each idea must meet all three of the minimum criteria and two of the extreme criteria.

|                  | Criteria 1                          | Criteria 2                           | Criteria 3                    |
| ---------------- | ----------------------------------- | ------------------------------------ | ----------------------------- |
| Minimum (3 of 3) | simplifies collaboration            | reduces the friction to contributing | aligned with market direction |
| Extreme (2 of 3) | enhances the ability to build trust | decreases manual effort              | increases clarity and purpose |

#### Target Audience

<!-- 
List the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->

Issue Boards are geared towards software development teams, but are also flexible enough for other teams in your organization to manage any type of tasks requiring tracking. We are optimizing Issue Boards for the following personas:

- [Parker (Product Manager)](/handbook/marketing/product-marketing/roles-personas/#parker-product-manager)
- [Delaney (Development Team Lead)](/handbook/marketing/product-marketing/roles-personas/#delaney-development-team-lead)
- [Presley (Product Designer)](/handbook/marketing/product-marketing/roles-personas/#presley-product-designer)
- [Sasha (Software Developer)](/handbook/marketing/product-marketing/roles-personas/#sasha-software-developer)
- [Devon (DevOps Engineer)](/handbook/marketing/product-marketing/roles-personas/#devon-devops-engineer)

#### Challenges to address

<!-- 
- What needs, goals, or jobs to be done do the users have?
- How do users address these challenges today? What products or work-arounds are utilized?

Provide links to UX Research issues, which validate these problems exist.
-->

- ~55% of teams use Scrum, ~10% use Kanban, 5% use ExtremeProgramming, and the rest a mixtures of the various core agile methodologies. First class support for all of these in a simple, convention over configuration manner within Issue Boards is non-trivial.
- Issues Boards are fairly useless from a "real time collaboration" standpoint. There is a large demand for solving this by building a Trello Powerup to enable [attaching issues to Trello cards](https://gitlab.com/gitlab-org/trello-power-up/issues/31).
- GitLab is currently not tracking the important metrics for Scrum and Kanban teams. Many customers have written custom scripts to import information from GitLab via the API into BI tools for reporting on common things like cumulative flow and velocioty.
  
### Where we are Headed

<!-- 
Describe the future state for your category. 
- What problems are we intending to solve? 
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized -->

We've written a [mock press release](../one_year_plan_portfolio_mgmt) describing where we intend to be by 2020-09-01. We will maintain this and update it as we sense and respond to our customers and the wider community.

#### What's Next & Why

<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

These are the epics that we will be working on over the next few releases:

- [Real-time Boards](https://gitlab.com/groups/gitlab-org/-/epics/382)
  - **Why:** To allow teams to collaborate in real time on Issue Boards to improve the efficiency and effectiveness of planning and staying in sync on who is working on what.
- [Integrated analytics on Issue Boards](https://gitlab.com/groups/gitlab-org/-/epics/1956)
  - **Why:** So that teams have burndown charts and cumulative flow diagrams specific to their team and workspace.
- [Custom Workflows](https://gitlab.com/groups/gitlab-org/-/epics/364)
  - **Why:** So teams can define a standard process and workflow that issues should flow through. This will help teams improve consistency, have a clear understanding of the current status of an issue, and is a pre-requisite to providing more advanced value stream reporting.

#### What is Not Planned Right Now

<!-- Often it's just as important to talk about what you're not doing as it is to 
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

#### Maturity Plan

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)). 

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

This category is currently at the 😊**Viable** maturity level, and our next maturity target is 😁**Complete** by 2020-06-22.

We are tracking our progress against this target via [this epic](https://gitlab.com/groups/gitlab-org/-/epics/965).

### User success metrics

<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->

We are currently using the [loose Stage Monthly Active Users (SMAU) definition](https://about.gitlab.com/handbook/product/categories/plan/#metrics) and intend on migrating to the strict definition as soon as we've implemented the necessary telemtry to measure the defined events.

### Why is this important?

<!--
- Why is GitLab building this feature? 
- Why impact will it have on the broader devops workflow?
- How confident are we? What is the effort?
-->

Issue Boards are the primary mechanism to keep teams aligned and executing on a shared, prioritized roadmap at the issue level. They also will provide the foundational constructs that allows teams to follow a consistent workflow and get quantitative data that empowers them to build trust through setting better expectations.

### Competitive Landscape

<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

The top competitor in this space is Atlassian's Jira, who are entrenched in many enterprise organizations that need an Agile/Kanban board solution. Atlassian also bought Trello, which is another significant player in this space, which has emphasized usability and being able to abstract out underlying software implementation details of an Agile sprint, to just simple task planning with a board interface.

Jira's and Trello's boards have inspired us to further refine and make our boards even more usable. In particular, we have the following ideas sketched and scoped out, including doing a lot more right in the board itself, without leaving it:

- [Combine board config and board filter for milestones](https://gitlab.com/groups/gitlab-org/-/epics/500)
- [Rows and columns design of boards - Remove mixed lists boards](https://gitlab.com/groups/gitlab-org/-/epics/616)
- [Epic swimlanes and filter in boards](https://gitlab.com/groups/gitlab-org/-/epics/328)
- [Edit issues without leaving board](https://gitlab.com/groups/gitlab-org/-/epics/383)

### Analyst Landscape

<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

Similar to [Project Management](../issue_tracking/), the analyst landscape is focused on enteprise agile planning and value stream management. Issue Boards are a means to further make these processes more refined and efficient. See:

- [Agile Portfolio Management](../../agile_portfolio_management/)
- [Value Stream Management](../../../manage/value_stream_management/)

### Top Customer Success/Sales issue(s)

<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

- [Overview of Issue Boards across ALL projects](https://gitlab.com/gitlab-org/gitlab/issues/15757) (👍 95)
- [Improve the efficiency and experience of weighting an issue backlog](https://gitlab.com/gitlab-org/gitlab/issues/13240) (👍 0)
- [Custom Workflows Per Group](https://gitlab.com/groups/gitlab-org/-/epics/364) (👍 5)

### Top user issue(s)

<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

- [Subissues on issue board](https://gitlab.com/gitlab-org/gitlab/issues/2530) (👍 89)
- [Create personal issue boards](https://gitlab.com/gitlab-org/gitlab/issues/18073) (👍 63)
- [Multiple projects issues on same Kanban/Board](https://gitlab.com/gitlab-org/gitlab/issues/17093) (👍 45)
- [Configure view when clicking on Issues tab to be issue board](https://gitlab.com/gitlab-org/gitlab/issues/15722) (👍 43)
- [Show tasks completed on Issue Card](https://gitlab.com/gitlab-org/gitlab/issues/16049) (👍 41)
- [Add Time Tracking column totals to Issue Board](https://gitlab.com/gitlab-org/gitlab/issues/18166) (👍 38)
- [Import/Export label configuration & Issue Board layout](https://gitlab.com/gitlab-org/gitlab/issues/20053) (👍 37)

### Top internal customer issue(s)

<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

- [Burndown chart in issue boards](https://gitlab.com/gitlab-org/gitlab/issues/6864) (👍 5)
- [Real time boards](https://gitlab.com/gitlab-org/gitlab/issues/16020) (👍 84)
- [Horizontal Swimlines](https://gitlab.com/groups/gitlab-org/-/epics/328) (👍 15)

### Top Strategy Item(s)

<!-- What's the most important thing to move your strategy forward?-->

A one year goal is to provide soft real time collaboration on Issues and Issue Boards. To get there, we need to:

- Implement [websockets](https://gitlab.com/gitlab-org/gitlab/issues/21249).
- Refactor Issue Boards to use GraphQL / Subscriptions to working with our websockets implementation. This will enable baseline [real-time baord lists](https://gitlab.com/groups/gitlab-org/-/epics/382).
- This will then open the door for allowing full interaction with issues from [within an Issue Board](https://gitlab.com/groups/gitlab-org/-/epics/383).

Another goal is to integrate analytics such as burndown charts and cumulative flow diagrams directly into Issue Boards. In order to do that, we need to:

- Introduce the ability to define consistent workflows. The working approach is to abstract the ability to do this from the currently proposed behavior for [customizing stages in cycle analytics reports](https://gitlab.com/gitlab-org/gitlab/issues/12196).
- Make a lot of improvements outlined in the `Category:Issue Tracking` direction that directly impact the quality of data we can show on an Issue Card.

We also want to provide a more seamless flow from top down planning all the way to the issue level. As we work on improving epics and our roadmapping capabilities, we will also need to focus on:

- Incorporating epics [more formally](https://gitlab.com/groups/gitlab-org/-/epics/328) into Issue Boards.
- Integrating [capacity planning and management](https://gitlab.com/groups/gitlab-org/-/epics/1686) into Issue Boards.
