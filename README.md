# www-gitlab-com

This is the source for the https://about.gitlab.com/ site. For a guide on how to start editing the website using git, see the [handbook page on that topic](https://about.gitlab.com/handbook/git-page-update).

## Local development

[Read how to preview any changes locally.](doc/development.md)

## Contributing

Please see the [contribution guidelines](CONTRIBUTING.md)

### Adding yourself to the team page

Edit [`data/team.yml`](./data/team.yml) and add a new entry for yourself (or
update the placeholder with your initials).

Images should be square and should be uploaded to [`source/images/team`](./source/images/team).

### Adding a pet to the team pets page

Edit [`data/pets.yml`](./data/pets.yml) and add a new entry.

Images should be uploaded to [`source/images/team/pets`](./source/images/team/pets).

### Blog posts

[Read how to add a blog post.](doc/blog-posts.md)

### Adding an application to the applications page (under `/applications`)

[How to add an application.](doc/applications.md)

### Updating the promotion link

This link appears at the top of the homepage and can be used to promote new
versions or upcoming events.

Edit [`data/promo.yml`](./data/promo.yml) to update the `link` and `text`
properties.

### Update the features page (under `/features`)

[How to update the features page.](doc/features.md#update-the-features-page-under-features)

### Create or update the comparison pages (under `/comparison`)

[How to update the comparison pages.](doc/features.md#create-or-update-the-comparison-pages-under-comparison)

### Update the releases page (under `/releases`)

[How to update the releases page.](doc/releases.md)

### Update the projects page (under `/handbook/engineering/projects`)

[How to update the projects page.](doc/projects.md)

### Press releases page

[How to add a press release page.](doc/press.md)

## PDF files

[How to work with PDFs.](doc/pdf.md)

## Production build

Before building the static files, ensure you have a GitLab.com `PRIVATE_TOKEN`
environment variable set up. This is required so that the Middleman can automatically
build the [direction page](https://about.gitlab.com/direction/).

```sh
bundle install
bundle exec rake build
# To also build PDFs:
bundle exec rake pdfs
```

The above command builds the static files and PDFs into the folder `public`.

Due to the number of API requests necessary for generating the Direction page,
the results of those requests are cached for 36 hours. If you need the page to
be updated earlier than that, provide the `CLEAR_DIRECTION_CACHE` [environment
variable](https://docs.gitlab.com/ee/ci/pipelines.html#manually-executing-pipelines).

## Custom Generators

There are a few custom, static generators specified in `config.rb`. For
example, some generators produce the direction issue list,
releases list, and organization chart dynamically.

These pages cannot be viewed directly via the Middleman server
(e.g. http://localhost:4567) because there are explicit rules that
tell Middleman to defer the generation to other scripts. These
special URLs (e.g. /releases/index.html) usually have two
Middleman keywords:

1. [`proxy`](https://middlemanapp.com/advanced/dynamic_pages/)

    This tells Middleman to output a static file based on the provided template.

2. [`ignore`](https://www.relishapp.com/middleman/middleman-core/docs/ignoring-paths)

    This tells the Middleman server not to handle this URL. The external generator will
    build static files.

To preview these custom-generated pages locally, you must first rebuild the files:

```sh
bundle exec middleman build
```

To test out the site, you must run another Web server from the
`public` directory:

```sh
(cd public; python -m SimpleHTTPServer 8000)
```

This will start a Web server on port 8000 (you may omit the port number). You can preview the site
by pointing your browser to http://localhost:8000.

## Review Apps

Thanks to the [Review Apps], the `www-gitlab-com` project supports live reviewing
of any website changes with every merge request. When a branch is pushed and
the pipeline is successfully run, you can see a link pointing to the live
environment in your merge request. The URL will be of the following scheme:
`<branch-name>.about.gitlab.com`.  Note that if you have underscores in your
branch name, they will be replaced with dashes in the URL.

Beware that:

- To successfully deploy a Review App, the branch must exist in the
  `www-gitlab-com` repository. That means that branches from forks will not
  deploy a Review App (hence MRs from contributors). For that case, you should
  have at least Developer access to the `www-gitlab-com` project or
  `gitlab-com` group.
- The generation of the direction, wishlist, and releases pages are omitted
  in branches and is run only on master. This helps to shave off some time from
  the build process. That means you won't be able to preview these pages with
  Review Apps.

[review apps]: https://docs.gitlab.com/ce/ci/review_apps/

## Use Docker to render the website

If you don't have a proper environment available to run Middleman, you can use
Docker to provide one for you locally on your machine. To have this
working correctly, you need to have at least Git and Docker installed, and an
internet connection available.

1. Clone the repository in a local folder
   ```sh
   git clone git@gitlab.com:gitlab-com/www-gitlab-com.git
   ```
>  **Note**: If your git clone is timing out, update the ServerAliveInterval in ~/.ssh/config to a larger number. For example, below is the content of the ~/.ssh/config file. <br> 
> ```
> Host *
>    ServerAliveInterval 1200
>    TCPKeepAlive yes 
>    IPQoS=throughput
>```

2. Create a Docker container
   ```sh
      docker create --name middleman -v "$PWD/www-gitlab-com":/site -w /site -p 4567:4567  
   -e LC_ALL=C.UTF-8 ruby:2.6 /bin/bash -c 'gem install bundler -v 1.17.3 && bundle install && bundle exec middleman'  
   ```

3. Start the container
   ```sh
   docker start middleman
   ```

4. Connect to http://localhost:4567

> **Note**: You won't be able to connect immediately. Middleman takes a few minutes to render the site. Run docker ps -ls to see if middleman is still running.

5. Change your original content as usual, and see the changes in the browser as soon as
you save the new version of the file (otherwise, just restart the container)

6. When you have finished, stop the container
   ```sh
   docker stop middleman
   ```

> **Note**: Subsequent runs will just require `docker start middleman`.

## Building a single file

If you only want Middleman to build a single file, you can do that via the `--glob` parameter.
Here are a few things to keep in mind:

* The glob parameter must match the **destination** file.

* The `--no-clean` option should be included or Middleman will wipe out
  files that do not match the glob parameter.

For example, here's how to rebuild the Contribute page. Note how
`source/company/culture/index.html.md.erb` is mapped to
`company/culture/contribute/index.html`:

```sh
bundle exec middleman build --glob={company/culture/contribute/index.html} --no-clean
```

For blog posts,
`source/blog/posts/2017-05-23-attributes-of-successful-development-teams.html.md`
maps to `blog/2017/05/23/attributes-of-successful-development-teams/index.html`:

```sh
bundle exec middleman build --glob={2017/05/23/attributes-of-successful-development-teams/index.html} --no-clean
```

## Conclusion (please leave this at the bottom of the doc)

In case someone forgot the most important commands and is catting this file from the command line we end by listing them:

```sh
kill -kill `lsof -t -i tcp:4567`
bundle exec rake new_post
open -a "Google Chrome" http://localhost:4567
bundle exec middleman
```

or to execute the last two commands just run:

```sh
bin/run
```

copy
